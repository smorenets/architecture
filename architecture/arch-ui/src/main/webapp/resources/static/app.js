'use strict';

var SocProjectApp = angular.module('SocProjectApp', [ 'ui.router', 'adminApp',
		'ngResource', 'treeGrid' ]);
SocProjectApp.config([ '$stateProvider', '$urlRouterProvider',
		'$locationProvider',
		function($stateProvider, $urlRouterProvider, $locationProvider) {
			/*
			 * $locationProvider.html5Mode({ enabled: true, requireBase: false
			 * });
			 */
			$urlRouterProvider.otherwise("/");
			$stateProvider.state('home', {
				url : '/',
				template : ''
			});
		} ]);

