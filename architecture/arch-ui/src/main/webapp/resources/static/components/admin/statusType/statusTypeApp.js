'use strict';

var StatusTypeApp = angular.module('admin_StatusTypeApp', ['ui.router', 'admin_StatusTypeController']);

StatusTypeApp.config(['$stateProvider', 'modalStateProvider', function ($stateProvider, modalStateProvider) {
    $stateProvider
        .state('admin.statusType', {
            url: '/status-types',
            controller: 'StatusTypeController', 
            views: {
                'status-types': {
             	   template: '<div data-ng-controller="StatusTypeController"><admin-directive info="statusType" typeHtml="List"/></div>',
                }
            }            
        });

    modalStateProvider.state('admin.statusType.new', {
        url: '/new/{parentId}',
        template:   '<admin-directive info="statusType" typeHtml="Edit"/>',
        controller: 'StatusTypeControllerEdit',
        data: {
            roles: []
        }
    });
    modalStateProvider.state('admin.statusType.edit', {
        url: '/edit/{id}',
        template: '<admin-directive info="statusType" typeHtml="Edit"/>',
        controller: 'StatusTypeControllerEdit',
        data: {
            roles: []
        }
    });
}]);
/*Factories*/

StatusTypeApp.factory('StatusTypeFactory', ['$resource', function ($resource) {

    return $resource('api/statustypes/:id', { id: '@_id' }, {
        update: {
            method: 'PUT'
          }
     });
}]);
