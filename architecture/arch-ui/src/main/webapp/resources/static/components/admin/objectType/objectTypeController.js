'use strict';

var admin_ObjectTypeController = angular.module('admin_ObjectTypeController', []);

admin_ObjectTypeController.controller('ObjTypeController', ['$scope','ObjectTypeFactory',

    function ($scope, ObjectTypeFactory) {
        console.log("Obj contr begin ");
        $scope.tableName = "Тип архитектурного объекта";
        $scope.objectTypes = [];

        $scope.$emit.refreshType= function(){
            console.log("Obj refresh");
            //$scope.objectTypes = ObjectTypeFactory.query();
            $scope.objectTypes = [
                {name:"ObjRefresh1", id : 1,
                    children:[
                        { name:"ObjChild1", id: 3 },
                        { name:"ObjChild2", id : 4 }
                    ] },
                { name:"ObjRefresh2",id : 2}
            ];
            $scope.entityTypes = $scope.objectTypes;
        };
        
        $scope.deleteType = function(id) {
            console.log("Delete Obj  ", id);
        	ObjectTypeFactory.delete({ 'id' : id }).$promise.then(function (result) {
                $scope.refresh();
        	});
        }
    }
]);

admin_ObjectTypeController.controller('ObjTypeControllerEdit', ['$scope', 
                  '$state', 'ObjectTypeFactory', '$stateParams', '$rootScope',
    function ($scope, $state, ObjectTypeFactory, $stateParams, $rootScope) {
        console.log("Edit Obj",$stateParams.parentId);
        if ($stateParams.id) {
            $scope.objectType = ObjectTypeFactory.get({ 'id' : $stateParams.id });
        } else if(!$stateParams.parentId){
            $scope.objectType = { 'id' : 0};
        } else {
            $scope.objectType = { 'id' : 0 , 'parent' : { 'id' :  $stateParams.parentId}};
        }

        $scope.close = function () {
            $state.go('^');
        };

        $scope.saveType = function () {
            console.log("save Obj ");
        	var response;
        	if($scope.objectType.id == 0) {
                response = ObjectTypeFactory.save($scope.objectType);
        	} else {
        		response = ObjectTypeFactory.update({ 'id' : $scope.objectType.id }, $scope.objectType);
        	}
        	response.$promise.then(function (result) {
        		$scope.$broadcast('type_refresh');
        		$scope.close();
        	});            
        }
    }
]);

