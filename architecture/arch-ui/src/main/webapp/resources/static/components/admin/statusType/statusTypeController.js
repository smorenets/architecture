'use strict';

var admin_StatusTypeController = angular.module('admin_StatusTypeController', []);

admin_StatusTypeController.controller('StatusTypeController', ['$scope', 'StatusTypeFactory',

    function ($scope, StatusTypeFactory) {
        $scope.tableName = "Тип статуса";
        $scope.statusTypes = [];

        $scope.$emit.refreshType= function(){
           // $scope.entityTypes = StatusTypeFactory.query();
            $scope.statusTypes = [
                {name:"statusRefresh1", id : 1,
                    children:[
                        { name:"statusChild1", id: 3 },
                        { name:"statusChild1", id : 4 }
                    ] },
                { name:"statusRefresh2",id : 2}
            ];
            $scope.entityTypes = $scope.statusTypes;
        };

        $scope.deleteType = function(id) {
        	console.log('delete status');
        	StatusTypeFactory.delete({ 'id' : id }).$promise.then(function (result) {
        	    $scope.refresh();
        	});
        }
    }
]);

admin_StatusTypeController.controller('StatusTypeControllerEdit', ['$scope', 
                  '$state', 'StatusTypeFactory', '$stateParams', '$rootScope',
    function ($scope, $state, StatusTypeFactory, $stateParams, $rootScope) {
        console.log("status edit", $stateParams.parentId);
        if ($stateParams.id) {
            $scope.statusType = StatusTypeFactory.get({ 'id' : $stateParams.id });
        } else if(!$stateParams.parentId){
        	$scope.statusType = { 'id' : 0};
        } else {
            $scope.statusType = { 'id' : 0 , 'parent' : { 'id' :  $stateParams.parentId}};
        }

        $scope.close = function () {
            $state.go('^');
        };

        $scope.saveType = function () {
        	var response;
        	if($scope.statusType.id == 0) {
                response = StatusTypeFactory.save($scope.statusType);
        	} else {
        		response = StatusTypeFactory.update({ 'id' : $scope.statusType.id }, $scope.statusType);
        	}
        	response.$promise.then(function (result) {
        		$rootScope.$broadcast('type_refresh');
        		$scope.close();
        	});            
        }
    }
]);

