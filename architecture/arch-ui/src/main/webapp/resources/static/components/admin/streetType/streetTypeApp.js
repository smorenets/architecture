'use strict';

var StreetTypeApp = angular.module('admin_StreetTypeApp', ['ui.router', 'admin_StreetTypeController']);

StreetTypeApp.config(['$stateProvider','modalStateProvider', function ($stateProvider, modalStateProvider) {
    $stateProvider
        .state('admin.streetType', {
            url: '/street-types',
            controller: 'StrTypeController',
            views: {
                'street-types': {
                    template: '<div data-ng-controller="StrTypeController"><admin-directive info="streetType" typeHtml="List"/></div>',
                }
            }
        });
    modalStateProvider.state('admin.streetType.new', {
        url: '/new/{parentId}',
        template: '<admin-directive info="streetType" typeHtml="Edit"/>',
        controller: 'StrTypeControllerEdit',
        data: {
            roles: []
        }
    });
    modalStateProvider.state('admin.streetType.edit', {
        url: '/edit/{id}',
        template: '<admin-directive info="streetType" typeHtml="Edit"/>',
        controller: 'StrTypeControllerEdit',
        data: {
            roles: []
        }
    });

}]);
/*Factories*/
/*----------------StreetType------------------*/
StreetTypeApp.factory('StreetTypeFactory', ['$resource', function ($resource) {

    return $resource('/api/streettypes/:id', { id: '@_id' }, {
        update: {
            method: 'PUT'
        }
    });
}]);
