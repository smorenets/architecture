
'use strict';

var admin_StreetTypeController = angular.module('admin_StreetTypeController', []);

admin_StreetTypeController.controller('StrTypeController', ['$scope', 'StreetTypeFactory',

    function ($scope, StreetTypeFactory) {
        $scope.tableName = "Тип архитектурного объекта";
        $scope.streetTypes = [];

        $scope.$emit.refreshType= function(){
            //$scope.streetTypes = StreetTypeFactory.query();
            console.log("refresh streeet");
            $scope.streetTypes = [
                {name:"Streetfresh1", id : 1,
                    children:[
                        { name:"StreetChild1", id: 3 },
                        { name:"StreetChild2", id : 4 }
                    ] },
                { name:"Streetfresh2",id : 2}
            ];
            $scope.entityTypes = $scope.streetTypes;
        }

        $scope.deleteType = function(id) {
            console.log("del street")
            StreetTypeFactory.delete({ 'id' : id }).$promise.then(function (result) {
                $scope.refresh();
            });
        }
    }
]);

admin_StreetTypeController.controller('StrTypeControllerEdit', ['$scope',
    '$state', 'StreetTypeFactory', '$stateParams', '$rootScope',
    function ($scope, $state, StreetTypeFactory, $stateParams, $rootScope) {
        console.log("edit street",$stateParams.parentId)

        if ($stateParams.id) {
            $scope.streetType = StreetTypeFactory.get({ 'id' : $stateParams.id });
        } else if(!$stateParams.parentId){
            $scope.streetType = { 'id' : 0};
        } else {
            $scope.streetType = { 'id' : 0 , 'parent' : { 'id' :  $stateParams.parentId}};
        }

        $scope.close = function () {
            $state.go('^');
        };

        $scope.saveType = function () {
            var response;
            if($scope.streetType.id == 0) {
                response = StreetTypeFactory.save($scope.streetType);
            } else {
                response = StreetTypeFactory.update({ 'id' : $scope.streetType.id }, $scope.streetType);
            }
            response.$promise.then(function (result) {
                $rootScope.$broadcast('type_refresh');
                $scope.close();
            });
        }
    }
]);


