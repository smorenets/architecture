'use strict';

var AdminApp = angular.module('adminApp',
		[ 'ui.router', 'admin_ObjectTypeApp', 'admin_StreetTypeApp', 'admin_StatusTypeApp' ]);
	AdminApp.directive('adminDirective', function() {
		return {
			restrict: 'AE',
			scope: true,
			//{
			//	entityType : "@info"
			//},
			//controller:function(scope,elem, attr){
			//	scope.test = attr.ctrlQ;
			//},
			templateUrl: function(elem, attr){
				return '/resources/static/components/admin/templates/templateType'+attr.typehtml+'.html';
			},
			controller: function($scope,$rootScope) {
				$scope.tree_data = [];

				$scope.expanding_property = {
					field: "name",
					displayName: "Название",
					sortable : true,
					filterable: true
				};

				$scope.col_defs = [
					{
						field : "id",
						displayName : " ",
						cellTemplate: "<a ui-sref='.edit({id : row.branch[col.field] })' class='btn btn-default' data-toggle='tooltip' title='Редактировать тип'><span	class='glyphicon glyphicon-pencil'></span></a>"
					},
					{
						field : "id",
						displayName : " ",
						cellTemplate: "<a ui-sref='.new({parentId : row.branch[col.field] })' class='btn btn-default' data-toggle='tooltip' title='Добавить дочерний тип'><span	class='glyphicon glyphicon-plus'></span></a>"
					},
					{
						field : "id",
						displayName : " ",
						cellTemplateScope: {
							click: function(data) {
								$scope.deleteType(data);
							}
						},
						cellTemplate: "<a ui-sref='.' ng-click='cellTemplateScope.click(row.branch[col.field])' class='btn btn-default' data-toggle='tooltip' title='Удалить тип статуса'><span	class='glyphicon glyphicon-remove'></span></a>"
					}

				];
				$rootScope.refresh = function() {

					console.log("Refresh");
					$scope.$emit.refreshType();
					$scope.tree_data = $scope.entityTypes;
				}

				$rootScope.$on('type_refresh', function (event, data) {
					console.log("On function");
					$rootScope.refresh();
				});

				$rootScope.refresh();
			},
			link: function (scope, element, attrs,ctrl) {
				console.log("link function");
				scope.entityType = scope.$eval(attrs.info);
			}

		};
	});

	AdminApp.config(
		[ '$stateProvider','modalStateProvider', function($stateProvider,modalStateProvider) {
			$stateProvider.state('admin', {
				url : '/admin',
				templateUrl : 'resources/static/components/admin/admin.html',
				controller : [ '$state', function($state) {
					$state.go('admin.objectType');
				}]
			});
		} ]);
