'use strict';

var ObjectTypeApp = angular.module('admin_ObjectTypeApp', ['ui.router', 'ui.bootstrap', 'admin_ObjectTypeController']);

ObjectTypeApp.provider('modalState', ['$stateProvider', function ($stateProvider) {
    var provider = this;
    this.$get = function () {
        return provider;
    }
    this.state = function (stateName, options) {
        var modalInstance;
        $stateProvider.state(stateName, {
            url: options.url,
            onEnter: ['$uibModal', '$state', function ($modal, $state) {
                modalInstance = $modal.open(options);
                modalInstance.result['finally'](function () {
                    modalInstance = null;
                    if ($state.$current.name === stateName) {
                        $state.go('^');
                    }
                });
            }],
            onExit: function () {
                if (modalInstance) {
                    modalInstance.close();
                }
            }
        });
    };
}
]);

ObjectTypeApp.config(['$stateProvider', 'modalStateProvider', function ($stateProvider, modalStateProvider) {
    $stateProvider
        .state('admin.objectType', {
            url: '/object-types',
            controller: 'ObjTypeController',
            views: {
                'object-types': {
             	   template: '<div data-ng-controller="ObjTypeController"><admin-directive info="objectType" typeHtml="List"/></div>',
                }
            }
        });

    modalStateProvider.state('admin.objectType.new', {
        url: '/new/{parentId}',
        template: '<admin-directive info="objectType" typeHtml="Edit"/>',
        controller: 'ObjTypeControllerEdit',
        data: {
            roles: []
        }
    });
    modalStateProvider.state('admin.objectType.edit', {
        url: '/edit/{id}',
        template: '<admin-directive info="objectType" typeHtml="Edit"/>',
        controller: 'ObjTypeControllerEdit',
        data: {
            roles: []
        }
    });
}]);
/*Factories*/
/*----------------ObjectType------------------*/
ObjectTypeApp.factory('ObjectTypeFactory', ['$resource', function ($resource) {

    return $resource('api/archobjecttypes/:id', { id: '@_id' }, {
        update: {
            method: 'PUT'
          }
     });
}]);
