package org.mycity.architecture.ui.initializer;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.FilterRegistration.Dynamic;

import org.mycity.architecture.ui.boot.ArchApplication;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * {@link AppInitializer} is starting-point for the web application inside web
 * container
 * 
 * @author admin
 * 
 */
@Order(value = Ordered.HIGHEST_PRECEDENCE)
public class AppInitializer implements WebApplicationInitializer {
	private static final String FILTER_ENCODING = "charEncodingFilter";

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		servletContext.setInitParameter("contextConfigLocation", "");
		
		WebApplicationContext context = getContext();
		servletContext.addListener(new ContextLoaderListener(context));
		//Support request-scoped beans
		servletContext.addListener(new RequestContextListener());

		initFilters(servletContext);
		initServlets(servletContext, context);
	}
	
	private void initFilters(ServletContext servletContext) {
		Dynamic dynamic = servletContext.addFilter(FILTER_ENCODING, CharacterEncodingFilter.class);
		dynamic.setInitParameter("encoding", "UTF-8");
		dynamic.setInitParameter("forceEncoding", "true");
		dynamic.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");
		
		Dynamic JPAFilter = servletContext.addFilter("openEntityManagerInViewFilter", OpenEntityManagerInViewFilter.class);
		JPAFilter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");
		
	}	

	private AnnotationConfigWebApplicationContext getContext() {
		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
		context.register(ArchApplication.class);
		return context;
	}
	
	private void initServlets(ServletContext servletContext, WebApplicationContext context) {
		ServletRegistration.Dynamic dispatcher = servletContext.addServlet("DispatcherServlet", new DispatcherServlet(
				context));
		dispatcher.setLoadOnStartup(1);
		dispatcher.addMapping("/*");

	}
	

}