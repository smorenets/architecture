package org.mycity.architecture.ui.boot;

import java.util.Collections;

import org.apache.catalina.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.boot.context.embedded.tomcat.TomcatContextCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
@EnableJpaRepositories("org.mycity.architecture")
@ComponentScan("org.mycity.architecture")
/**
 * Entry point to start Spring Boot Application
 * 
 * @author admin
 *
 */
public class ArchApplication extends WebMvcConfigurerAdapter implements EmbeddedServletContainerCustomizer {

	@Autowired
	private OpenEntityManagerInViewFilter openEntityManagerInViewFilter;

	public static void main(String[] args) {
		SpringApplication.run(ArchApplication.class, args);
	}

	@Bean
	public FilterRegistrationBean openEntityManagerInViewFilterRegistration() {
		FilterRegistrationBean filter = new FilterRegistrationBean(openEntityManagerInViewFilter);
		filter.setUrlPatterns(Collections.singletonList("/*"));
		return filter;
	}

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	@Override
	public void customize(ConfigurableEmbeddedServletContainer container) {
		((TomcatEmbeddedServletContainerFactory) container).addContextCustomizers(new TomcatContextCustomizer() {
			@Override
			public void customize(Context context) {
				context.addWelcomeFile("/index.html");
			}
		});
	}

}
