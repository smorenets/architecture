package org.mycity.architecture.service.controller;

import static org.hamcrest.Matchers.*;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mycity.architecture.model.entity.OldFunction;
import org.mycity.architecture.service.config.TestServiceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Class provides methods for testing behavior of OldFunctionController.
 * 
 * @author Sergey Tishchenko
 * @version 0.1
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestServiceConfig.class)
@WebAppConfiguration
public class OldFunctionControllerTest {

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private OpenEntityManagerInViewFilter openEntityManagerInViewFilter;
	
	private MockMvc mockMvc;
	private ObjectMapper jsonMapper = new ObjectMapper();

	private final String preUri = "http://localhost";
	private final String mainUri = "/api/oldfunctions/";

	@Before
	public void setup() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(openEntityManagerInViewFilter, "/*").build();
		deleteAll();
	}

	// auxiliary method for creating object which return id.
	// throws AssertionError if something going wrong.
	private int create(OldFunction obj) throws Exception {
		String bodyStr = jsonMapper.writeValueAsString(obj);
		System.out.println(bodyStr);

		MockHttpServletRequestBuilder request = post(mainUri).contentType(MediaType.APPLICATION_JSON_UTF8).content(bodyStr);

		String location = mockMvc.perform(request).andExpect(status().isCreated())
				.andExpect(header().string("Location", startsWith(preUri + mainUri))).andExpect(content().string("")).andReturn()
				.getResponse().getHeader("Location");

		int id = -1;
		try {
			id = Integer.parseInt(location.substring(location.lastIndexOf('/') + 1));
			System.out.println(id);
		} catch (Exception ex) {
			Assert.fail("wrong id in Location header");
		}
		;

		return id;
	}

	// auxiliary method for deleting all
	private void deleteAll() throws Exception {
		mockMvc.perform(delete(mainUri)).andExpect(status().isNoContent());
	}

	// successful entity creation
	@Test
	public void testCreateSuccess() throws Exception {
		OldFunction obj = new OldFunction();
		obj.setName("OldFunction");
		create(obj);
	}

	// malformed body
	@Test
	public void testCreateMalformedBodyBadRequest() throws Exception {
		String bodyStr = "{";
		System.out.println(bodyStr);

		MockHttpServletRequestBuilder request = post(mainUri).contentType(MediaType.APPLICATION_JSON_UTF8).content(bodyStr);

		mockMvc.perform(request).andExpect(status().isBadRequest());

	}

	// 'id' in URL does not equal 'id' in body
	@Test
	public void testCreateIdNotNullBadRequest() throws Exception {
		OldFunction obj = new OldFunction();
		obj.setId(10);
		obj.setName("OldFunction");

		mockMvc.perform(post(mainUri).contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsString(obj))).andExpect(
				status().isBadRequest());
	}

	// create and get entity
	@Test
	public void testCreateAndGetSuccess() throws Exception {
		// create entity
		OldFunction obj = new OldFunction();
		obj.setName("OldFunction");
		int id = create(obj);
		obj.setId(id);

		// get entity
		MockHttpServletRequestBuilder request = get(mainUri + id).accept(MediaType.APPLICATION_JSON_UTF8);

		mockMvc.perform(request).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("id", is(obj.getId()))).andExpect(jsonPath("name", is(obj.getName())));
	}

	// get non-existent entity
	@Test
	public void testGetNotFound() throws Exception {
		int id = 100000;
		mockMvc.perform(get(mainUri + id)).andExpect(status().isNotFound()).andExpect(content().string(""));
	}

	// delete non-existent entity
	@Test
	public void testDeleteNotFound() throws Exception {
		int id = 100000;
		mockMvc.perform(delete(mainUri + id)).andExpect(status().isNotFound()).andExpect(content().string(""));
	}

	// create entity and delete it
	@Test
	public void testCreateAndDeleteSuccess() throws Exception {
		// create entity
		OldFunction obj = new OldFunction();
		obj.setName("OldFunction");
		int id = create(obj);
		obj.setId(id);

		// delete entity
		mockMvc.perform(delete(mainUri + id)).andExpect(status().isNoContent()).andExpect(content().string(""));

		// get no entity
		mockMvc.perform(get(mainUri + id)).andExpect(status().isNotFound()).andExpect(content().string(""));
	}

	// malformed body
	@Test
	public void testCreateAndUpdateMalformedBodyBadRequest() throws Exception {
		// create entity
		OldFunction obj = new OldFunction();
		obj.setName("OldFunction");
		int id = create(obj);
		obj.setId(id);

		// update malformed body
		String bodyStr = "{";
		System.out.println(bodyStr);

		MockHttpServletRequestBuilder request = put(mainUri + id).contentType(MediaType.APPLICATION_JSON_UTF8).content(bodyStr);

		mockMvc.perform(request).andExpect(status().isBadRequest());

	}

	// update entity
	@Test
	public void testCreateAndUpdateSuccess() throws Exception {
		// create entity
		OldFunction obj = new OldFunction();
		obj.setName("OldFunction");
		int id = create(obj);
		obj.setId(id);

		// update - change name
		obj.setName("new_OldFunction");
		obj.setCreatedAt(new Date());
		mockMvc.perform(put(mainUri + id).contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsString(obj)))
				.andExpect(status().isOk()).andExpect(jsonPath("id", is(obj.getId()))).andExpect(jsonPath("name", is(obj.getName())));

		// get changed entity
		MockHttpServletRequestBuilder request = get(mainUri + id).accept(MediaType.APPLICATION_JSON_UTF8);

		mockMvc.perform(request).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("id", is(obj.getId()))).andExpect(jsonPath("name", is(obj.getName())));
	}

	// 'id' in URL does not equal 'id' in body
	@Test
	public void testUpdateIdConflictBadRequest() throws Exception {
		int id = 100000;
		OldFunction obj = new OldFunction();
		obj.setName("OldFunction");

		mockMvc.perform(put(mainUri + id).contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsString(obj)))
				.andExpect(status().isBadRequest());
	}

	// update non-existent entity
	@Test
	public void testUpdateNotFound() throws Exception {
		int id = 100000;
		OldFunction obj = new OldFunction();
		obj.setId(id);
		obj.setName("OldFunction");

		mockMvc.perform(put(mainUri + id).contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsString(obj)))
				.andExpect(status().isNotFound()).andExpect(content().string(""));
	}

	@Test
	public void testGetAllSuccess() throws Exception {
		// create three entities
		OldFunction obj1 = new OldFunction();
		obj1.setName("OldFunction1");

		OldFunction obj2 = new OldFunction();
		obj2.setName("OldFunction2");

		OldFunction obj3 = new OldFunction();
		obj3.setName("OldFunction3");

		// create the first
		int id1 = create(obj1);
		obj1.setId(id1);

		// create the second
		int id2 = create(obj2);
		obj2.setId(id2);

		// create the third
		int id3 = create(obj3);
		obj3.setId(id3);

		// get all
		mockMvc.perform(get(mainUri).accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(jsonPath("$[0]id", is(obj1.getId())))
				.andExpect(jsonPath("$[0]name", is(obj1.getName()))).andExpect(jsonPath("$[1]id", is(obj2.getId())))
				.andExpect(jsonPath("$[1]name", is(obj2.getName()))).andExpect(jsonPath("$[2]id", is(obj3.getId())))
				.andExpect(jsonPath("$[2]name", is(obj3.getName())));

	}

	@Test
	public void testGetAllNoContent() throws Exception {
		mockMvc.perform(get(mainUri).accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk()).andExpect(content().string("[]"));
	}

}
