package org.mycity.architecture.service.controller;

import java.nio.charset.Charset;
import java.util.Date;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mycity.architecture.model.entity.ArchObjectType;
import org.mycity.architecture.service.config.TestServiceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;

/**
 * @author Nikolay Koretskyy
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestServiceConfig.class)
@WebAppConfiguration
public class ArchObjectTypeControllerTest2 {

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	private MockMvc mockMvc;
	
	private ObjectMapper jsonMapper = new ObjectMapper();

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private OpenEntityManagerInViewFilter openEntityManagerInViewFilter;

	private final String preUri = "http://localhost";
	private final String mainUri = "/api/archobjecttypes";

	// Auxiliary method for creation object which return id. Throws AssertionError if something going wrong.
	private int create(ArchObjectType obj) throws Exception {
		String bodyStr = jsonMapper.writeValueAsString(obj);
		System.out.println(bodyStr);

		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(mainUri)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(bodyStr);

		String location = mockMvc.perform(request)
				.andReturn().getResponse().getHeader("Location");

		int id = -1;
		try {
			id = Integer.parseInt(location.substring(location.lastIndexOf('/') + 1));
			System.out.println(id);
		} catch (Exception ex) {
			Assert.fail("Wrong id in Location header");
		}

		return id;
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(openEntityManagerInViewFilter, "/*").build();

		mockMvc.perform(delete(mainUri));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetSuccess() throws Exception {
		int id = create(new ArchObjectType("Усадьба"));
		mockMvc.perform(get(mainUri + "/" + id))
				.andExpect(status().isOk())
				.andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$.id", is(id)))
				.andExpect(jsonPath("$.name", is("Усадьба")));
	}

	@Test
	public void testGetNotSuccess() throws Exception {
		mockMvc.perform(get(mainUri + "/100"))
				.andExpect(status().isNotFound())
				.andExpect(content().string(""));
	}

	@Test
	public void testGetAllSuccess() throws Exception {

		int id1 = create(new ArchObjectType("Усадьба"));
		int id2 = create(new ArchObjectType("Доходный дом"));
		int id3 = create(new ArchObjectType("Фабрика-кухня"));
		int id4 = create(new ArchObjectType("Магазин"));

		mockMvc.perform(get(mainUri))
				.andExpect(status().isOk())
				.andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$", hasSize(4)))
				.andExpect(jsonPath("$[0].id", is(id1)))
				.andExpect(jsonPath("$[0].name", is("Усадьба")))
				.andExpect(jsonPath("$[1].id", is(id2)))
				.andExpect(jsonPath("$[1].name", is("Доходный дом")))
				.andExpect(jsonPath("$[2].id", is(id3)))
				.andExpect(jsonPath("$[2].name", is("Фабрика-кухня")))
				.andExpect(jsonPath("$[3].id", is(id4)))
				.andExpect(jsonPath("$[3].name", is("Магазин")));
	}

	@Test
	public void testGetAllEmptySuccess() throws Exception {
		mockMvc.perform(get(mainUri))
				.andExpect(status().isOk())
				.andExpect(content().string("[]"));
	}

	@Test
	public void testCreateSuccess() throws Exception {
		String archObjectTypeJson = jsonMapper.writeValueAsString(new ArchObjectType("Гостиница"));
		mockMvc.perform(post(mainUri)
				.contentType(contentType)
				.content(archObjectTypeJson))
				.andExpect(status().isCreated())
				.andExpect(MockMvcResultMatchers.header().string("Location", Matchers.startsWith(preUri + mainUri)))
				.andExpect(content().string(""));

		mockMvc.perform(get(mainUri))
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].name", is("Гостиница")))
				.andExpect(jsonPath("$[0].createdAt", notNullValue()));
	}

	@Test
	public void testCreateNotSuccessConflict() throws Exception {
		int id = create(new ArchObjectType("Ресторан"));
		// The idea is that we already have ArchObjectType with this id (for ORM) or name (for Map), so it should't be created.
		String archObjectTypeJson = jsonMapper.writeValueAsString(new ArchObjectType(id, "Ресторан"));
		mockMvc.perform(post(mainUri)
				.contentType(contentType)
				.content(archObjectTypeJson))
				.andExpect(status().isConflict())
				.andExpect(content().string(""));

		mockMvc.perform(get(mainUri))
				.andExpect(jsonPath("$", hasSize(1)));
	}

	@Test
	public void testCreateNotSuccessBadRequest() throws Exception {
		String archObjectTypeJson = jsonMapper.writeValueAsString(new ArchObjectType(300, "Ресторан"));
		mockMvc.perform(post(mainUri)
				.contentType(contentType)
				.content(archObjectTypeJson))
				.andExpect(status().isBadRequest())
				.andExpect(content().string(""));
	}

	@Test
	public void testUpdateSuccess() throws Exception {
		int id = create(new ArchObjectType("Особняк"));
		ArchObjectType archObjectType = new ArchObjectType(id, "Особняк измененный");
		archObjectType.setCreatedAt(new Date());
		String archObjectTypeJson = jsonMapper.writeValueAsString(archObjectType);
		mockMvc.perform(put(mainUri + "/" + id)
				.contentType(contentType)
				.content(archObjectTypeJson))
				.andExpect(status().isOk())
				.andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$.id", is(id)))
				.andExpect(jsonPath("$.name", is("Особняк измененный")))
				.andExpect(jsonPath("$.modifiedAt", notNullValue()));

		mockMvc.perform(get(mainUri + "/" + id))
				.andExpect(jsonPath("$.id", is(id)))
				.andExpect(jsonPath("$.name", is("Особняк измененный")));
	}

	@Test
	public void testUpdateNotSuccessNotFound() throws Exception {
		String archObjectTypeJson = jsonMapper.writeValueAsString(new ArchObjectType(300, "Особняк измененный"));
		mockMvc.perform(put(mainUri + "/300")
				.contentType(contentType)
				.content(archObjectTypeJson))
				.andExpect(status().isNotFound())
				.andExpect(content().string(""));
	}

	@Test
	public void testUpdateNotSuccessBadRequest() throws Exception {
		int id = create(new ArchObjectType("Особняк"));
		String archObjectTypeJson = jsonMapper.writeValueAsString(new ArchObjectType("Особняк измененный"));
		mockMvc.perform(put(mainUri + "/" + id)
				.contentType(contentType)
				.content(archObjectTypeJson))
				.andExpect(status().isBadRequest())
				.andExpect(content().string(""));
	}

	@Test
	public void testDeleteSuccess() throws Exception {
		int id = create(new ArchObjectType("Особняк"));
		mockMvc.perform(delete(mainUri + "/" + id))
				.andExpect(status().isNoContent())
				.andExpect(content().string(""));

		mockMvc.perform(get(mainUri + "/" + id))
				.andExpect(status().isNotFound())
				.andExpect(content().string(""));
	}

	@Test
	public void testDeleteNotSuccess() throws Exception {
		mockMvc.perform(delete(mainUri + "/400"))
				.andExpect(status().isNotFound())
				.andExpect(content().string(""));
	}

	@Test
	public void testDeleteAllSuccess() throws Exception {

		create(new ArchObjectType("Усадьба"));
		create(new ArchObjectType("Доходный дом"));
		create(new ArchObjectType("Фабрика-кухня"));
		create(new ArchObjectType("Магазин"));
		
		mockMvc.perform(delete(mainUri))
				.andExpect(status().isNoContent())
				.andExpect(content().string(""));

		mockMvc.perform(get(mainUri))
				.andExpect(status().isOk())
				.andExpect(content().string("[]"));
	}

}