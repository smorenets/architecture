package org.mycity.architecture.service.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableJpaRepositories("org.mycity.architecture")
@ComponentScan("org.mycity.architecture")
@EnableWebMvc
public class TestServiceConfig {

}
