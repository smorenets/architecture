package org.mycity.architecture.service.controller;

import org.mycity.architecture.model.entity.Article;
import org.mycity.architecture.persistence.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * CRUD REST-services for Article entity
 */

@RestController
@RequestMapping(value = "/api/articles")
public class ArticleController {

    @Autowired
    private ArticleRepository repository;

    /*
     * Retrieve all articles
     */
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<Article>> getAll() {
        System.out.println("Fetching all articles");

        List<Article> articles = (List<Article>) repository.findAll();

        /*
         * Fill article text with data
         */
        for (Article article : articles) {
            ArticleParser parser = new ArticleParserImpl();
            article.setText(parser.parse(article));
        }

        return new ResponseEntity<>(articles, HttpStatus.OK);
    }

    /*
     * Retrieve single article by id
	 */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Article> get(@PathVariable("id") int id) {

        System.out.println("Fetching article with id " + id);

        Article article = repository.findOne(id);
        if (article == null) {
            System.out.println("Article with id " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        /*
         * Fill article text with data
         */
        ArticleParser parser = new ArticleParserImpl();
        article.setText(parser.parse(article));

        return new ResponseEntity<>(article, HttpStatus.OK);
    }

    /*
     * Create an article
     */
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Void> create(@Valid @RequestBody Article article, UriComponentsBuilder ucBuilder) {

        System.out.println("Creating article " + article.getName());

        if (repository.exists(article.getId())) {
            System.out.println("An article with id " + article.getId() + " already exist");
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        if (article.getId() != 0) {
            System.out.println("Id from the request body must be null or 0");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        article.setCreatedAt(new Date());
        article = repository.save(article);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/article/{id}").buildAndExpand(article.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    /*
     * Update an article
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Article> update(@PathVariable("id") int id, @Valid @RequestBody Article article) {

        System.out.println("Updating article " + id);

        if (article.getId() != id) {
            System.out.println("Id from the request body must be equal to id from URL");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Article articleOld = repository.findOne(id);
        if (articleOld == null) {
            System.out.println("Article with id " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            article.setCreatedAt(articleOld.getCreatedAt());
            article.setCreatedBy(articleOld.getCreatedBy());
            article.setModifiedAt(new Date());
            // article.setModifiedBy(new User());

            article = repository.save(article);
            return new ResponseEntity<>(article, HttpStatus.OK);
        }
    }

    /*
     * Delete an article
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Article> delete(@PathVariable("id") int id) {

        System.out.println("Fetching & deleting article with id " + id);

        if (!repository.exists(id)) {
            System.out.println("Unable to delete. article with id " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        repository.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /*
     * Delete all articles
     */
    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity<Article> deleteAll() {

        System.out.println("Deleting all articles");
        repository.deleteAll();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
