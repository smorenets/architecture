package org.mycity.architecture.service.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.mycity.architecture.model.entity.ArchObjectType;
import org.mycity.architecture.persistence.repository.ArchObjectTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * This is JPA version of ArchObjectTypeController.
 * 
 * @author Nikolay Koretskyy
 *
 */
@RestController
@RequestMapping(value = "/api/archobjecttypes")
public class ArchObjectTypeController {
	
	@Autowired
	private ArchObjectTypeRepository repository;

	/*
	 * Retrieve all archObjectTypes
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<ArchObjectType>> getAll() {

		System.out.println("Fetching all archObjectTypes");

		List<ArchObjectType> archObjectTypes = (List<ArchObjectType>) repository.findAll();

		return new ResponseEntity<List<ArchObjectType>>(archObjectTypes, HttpStatus.OK);
	}

	/*
	 * Retrieve single archObjectType
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ArchObjectType> get(@PathVariable("id") int id) {

		System.out.println("Fetching archObjectType with id " + id);

		ArchObjectType archObjectType = repository.findOne(id);
		if (archObjectType == null) {
			System.out.println("ArchObjectType with id " + id + " not found");
			return new ResponseEntity<ArchObjectType>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ArchObjectType>(archObjectType, HttpStatus.OK);
	}

	/*
	 * Create an archObjectType
	 */
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Void> create(@Valid @RequestBody ArchObjectType archObjectType, UriComponentsBuilder ucBuilder) {

		System.out.println("Creating archObjectType " + archObjectType.getName());

		if (repository.exists(archObjectType.getId())) {
			System.out.println("An archObjectType with id " + archObjectType.getId() + " already exist");
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}

		if (archObjectType.getId() != 0) {
			System.out.println("Id from the request body must be null or 0");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}

		archObjectType.setCreatedAt(new Date());
		// archObjectType.setCreatedBy(new User());

		archObjectType = repository.save(archObjectType);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/api/archobjecttypes/{id}").buildAndExpand(archObjectType.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	/*
	 * Update an archObjectType
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ArchObjectType> update(@PathVariable("id") int id, @Valid @RequestBody ArchObjectType archObjectType) {

		System.out.println("Updating archObjectType " + id);

		if (archObjectType.getId() != id) {
			System.out.println("Id from the request body must be equal to id from URL");
			return new ResponseEntity<ArchObjectType>(HttpStatus.BAD_REQUEST);
		}

		ArchObjectType archObjectTypeOld = repository.findOne(id);
		if (archObjectTypeOld == null) {
			System.out.println("ArchObjectType with id " + id + " not found");		
			return new ResponseEntity<ArchObjectType>(HttpStatus.NOT_FOUND);			
		}
		else {
			archObjectType.setParent(archObjectTypeOld.getParent());
			archObjectType.setCreatedAt(archObjectTypeOld.getCreatedAt());
			archObjectType.setCreatedBy(archObjectTypeOld.getCreatedBy());
			archObjectType.setModifiedAt(new Date());
			// archObjectType.setModifiedBy(new User());

			archObjectType = repository.save(archObjectType);
			return new ResponseEntity<ArchObjectType>(archObjectType, HttpStatus.OK);
		}	
	}

	/*
	 * Delete an archObjectType
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<ArchObjectType> delete(@PathVariable("id") int id) {

		System.out.println("Fetching & deleting archObjectType with id " + id);

		if (!repository.exists(id)) {
			System.out.println("Unable to delete. ArchObjectType with id " + id + " not found");
			return new ResponseEntity<ArchObjectType>(HttpStatus.NOT_FOUND);
		}

		repository.delete(id);
		return new ResponseEntity<ArchObjectType>(HttpStatus.NO_CONTENT);
	}

	/*
	 * Delete all archObjectTypes
	 */
	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<ArchObjectType> deleteAll() {

		System.out.println("Deleting all archObjectTypes");
		repository.deleteAll();
		return new ResponseEntity<ArchObjectType>(HttpStatus.NO_CONTENT);
	}
}
