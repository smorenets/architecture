package org.mycity.architecture.service.controller;

import org.mycity.architecture.model.entity.ArchObject;
import org.mycity.architecture.persistence.repository.ArchObjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * CRUD REST-services for ArchObject entity
 *
 */

@RestController
@RequestMapping(value = "/api/archobjects")
public class ArchObjectController {

        @Autowired
        private ArchObjectRepository repository;

        /*
         * Retrieve all archObjects
         */
        @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
        public ResponseEntity<List<ArchObject>> getAll() {
            System.out.println("Fetching all archObjects");

            List<ArchObject> archObjects = (List<ArchObject>) repository.findAll();

            return new ResponseEntity<>(archObjects, HttpStatus.OK);
        }

        /*
         * Retrieve single archObject by id
         */
        @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
        public ResponseEntity<ArchObject> get(@PathVariable("id") int id) {

            System.out.println("Fetching archObject with id " + id);

            ArchObject archObject = repository.findOne(id);
            if (archObject == null) {
                System.out.println("ArchObject with id " + id + " not found");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(archObject, HttpStatus.OK);
        }

        /*
         * Create an archObject
         */
        @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
        public ResponseEntity<Void> create(@Valid @RequestBody ArchObject archObject, UriComponentsBuilder ucBuilder) {

            System.out.println("Creating archObject " + archObject.getName());

            if (repository.exists(archObject.getId())) {
                System.out.println("An archObject with id " + archObject.getId() + " already exist");
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }
            if (archObject.getId() != 0) {
                System.out.println("Id from the request body must be null or 0");
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            archObject.setCreatedAt(new Date());
            archObject = repository.save(archObject);

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(ucBuilder.path("/api/archObject/{id}").buildAndExpand(archObject.getId()).toUri());
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        }

        /*
         * Update an archObject
         */
        @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
        public ResponseEntity<ArchObject> update(@PathVariable("id") int id, @Valid @RequestBody ArchObject archObject) {

            System.out.println("Updating archObject " + id);

            if (archObject.getId() != id) {
                System.out.println("Id from the request body must be equal to id from URL");
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            ArchObject archObjectOld = repository.findOne(id);
            if (archObjectOld == null) {
                System.out.println("ArchObject with id " + id + " not found");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            else {
                archObject.setCreatedAt(archObjectOld.getCreatedAt());
                archObject.setCreatedBy(archObjectOld.getCreatedBy());
                archObject.setModifiedAt(new Date());
                // archObject.setModifiedBy(new User());

                archObject = repository.save(archObject);
                return new ResponseEntity<>(archObject, HttpStatus.OK);
            }
        }

        /*
         * Delete an archObject
         */
        @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
        public ResponseEntity<ArchObject> delete(@PathVariable("id") int id) {

            System.out.println("Fetching & deleting archObject with id " + id);

            if (!repository.exists(id)) {
                System.out.println("Unable to delete. archObject with id " + id + " not found");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            repository.delete(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        /*
         * Delete all archObjects
         */
        @RequestMapping(method = RequestMethod.DELETE)
        public ResponseEntity<ArchObject> deleteAll() {

            System.out.println("Deleting all archObjects");
            repository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

}
