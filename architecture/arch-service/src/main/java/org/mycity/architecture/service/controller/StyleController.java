package org.mycity.architecture.service.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.mycity.architecture.model.entity.Style;
import org.mycity.architecture.persistence.repository.StyleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * Rest Controller for Style entity.
 * 
 * @author Sergey Tishchenko
 *
 */
@RestController
@RequestMapping("/api/styles")
public class StyleController {

	@Autowired
	private StyleRepository repository;

	/**
	 * Get all Styles.
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Style>> getAll() {
		List<Style> styles = (List<Style>) repository.findAll();
		return new ResponseEntity<List<Style>>(styles, HttpStatus.OK);
	}

	/**
	 * Get Style by id.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Style> get(@PathVariable int id) {
		Style style = repository.findOne(id);
		if (style == null) {
			return new ResponseEntity<Style>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Style>(style, HttpStatus.OK);
	}

	/**
	 * Delete Style by id.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable int id) {
		if (!repository.exists(id)) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		repository.delete(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Delete all Styles.
	 */
	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteAll() {
		repository.deleteAll();
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Update existing Style.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> update(@PathVariable int id, @Valid @RequestBody Style style) {
		if (style.getId() != id) {
			return new ResponseEntity<String>("{\"message\":\"id from URL must be equal to id from the request body\"}",
					HttpStatus.BAD_REQUEST);
		}

		Style styleOld = repository.findOne(id);
		if (styleOld == null) {
			System.out.println("ArchObjectType with id " + id + " not found");		
			return new ResponseEntity<Style>(HttpStatus.NOT_FOUND);			
		}
		else {
			style.setParent(styleOld.getParent());
			style.setCreatedAt(styleOld.getCreatedAt());
			style.setCreatedBy(styleOld.getCreatedBy());
			style.setModifiedAt(new Date());
			// style.setModifiedBy(new User());
	
			style = repository.save(style);
			return new ResponseEntity<Style>(style, HttpStatus.OK);
		}	
	}

	/**
	 * Create Style.
	 */
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> create(@Valid @RequestBody Style style) {
		if (style.getId() != 0) {
			return new ResponseEntity<String>("{\"message\":\"id must be null or 0\"}", HttpStatus.BAD_REQUEST);
		}
		style.setCreatedAt(new Date());
		style = repository.save(style);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(style.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

}
