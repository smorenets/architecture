package org.mycity.architecture.service.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.mycity.architecture.model.entity.PersonType;
import org.mycity.architecture.persistence.repository.PersonTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * Rest Controller for PersonType entity.
 * 
 * @author Sergey Tishchenko
 *
 */
@RestController
@RequestMapping("/api/persontypes")
public class PersonTypeController {

	@Autowired
	private PersonTypeRepository repository;

	/**
	 * Get all PersonTypes.
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<PersonType>> getAll() {
		List<PersonType> personTypes = (List<PersonType>) repository.findAll();
		return new ResponseEntity<List<PersonType>>(personTypes, HttpStatus.OK);
	}

	/**
	 * Get PersonType by id.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<PersonType> get(@PathVariable int id) {
		PersonType personType = repository.findOne(id);
		if (personType == null) {
			return new ResponseEntity<PersonType>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<PersonType>(personType, HttpStatus.OK);
	}

	/**
	 * Delete PersonType by id.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable int id) {
		if (!repository.exists(id)) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		repository.delete(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Delete all PersonTypes.
	 */
	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteAll() {
		repository.deleteAll();
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Update existing PersonType.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> update(@PathVariable int id, @Valid @RequestBody PersonType personType) {
		if (personType.getId() != id) {
			return new ResponseEntity<String>("{\"message\":\"id from URL must be equal to id from the request body\"}",
					HttpStatus.BAD_REQUEST);
		}

		PersonType personTypeOld = repository.findOne(id);
		if (personTypeOld == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);			
		}
		else {
			personType.setParent(personTypeOld.getParent());
			personType.setCreatedAt(personTypeOld.getCreatedAt());
			personType.setCreatedBy(personTypeOld.getCreatedBy());
			personType.setModifiedAt(new Date());
			// personType.setModifiedBy(new User());

			personType = repository.save(personType);
			return new ResponseEntity<PersonType>(personType, HttpStatus.OK);
		}
	}

	/**
	 * Create PersonType.
	 */
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> create(@Valid @RequestBody PersonType personType) {
		if (personType.getId() != 0) {
			return new ResponseEntity<String>("{\"message\":\"id must be null or 0\"}", HttpStatus.BAD_REQUEST);
		}
		personType.setCreatedAt(new Date());
		personType = repository.save(personType);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(personType.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

}
