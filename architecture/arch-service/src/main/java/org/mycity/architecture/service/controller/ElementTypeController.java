package org.mycity.architecture.service.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.mycity.architecture.model.entity.ElementType;
import org.mycity.architecture.persistence.repository.ElementTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * Rest Controller for ElementType entity.
 * 
 * @author Sergey Tishchenko
 *
 */
@RestController
@RequestMapping("/api/elementtypes")
public class ElementTypeController {

	@Autowired
	private ElementTypeRepository repository;

	/**
	 * Get all ElementTypes.
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<ElementType>> getAll() {
		List<ElementType> elementTypes = (List<ElementType>) repository.findAll();
		return new ResponseEntity<List<ElementType>>(elementTypes, HttpStatus.OK);
	}

	/**
	 * Get ElementType by id.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ElementType> get(@PathVariable int id) {
		ElementType elementType = repository.findOne(id);
		if (elementType == null) {
			return new ResponseEntity<ElementType>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ElementType>(elementType, HttpStatus.OK);
	}

	/**
	 * Delete ElementType by id.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable int id) {
		if (!repository.exists(id)) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		repository.delete(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Delete all ElementTypes.
	 */
	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteAll() {
		repository.deleteAll();
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Update existing ElementType.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> update(@PathVariable int id, @Valid @RequestBody ElementType elementType) {
		if (elementType.getId() != id) {
			return new ResponseEntity<String>("{\"message\":\"id from URL must be equal to id from the request body\"}",
					HttpStatus.BAD_REQUEST);
		}

		ElementType elementTypeOld = repository.findOne(id);
		if (elementTypeOld == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);			
		}
		else {
			elementType.setParent(elementTypeOld.getParent());
			elementType.setCreatedAt(elementTypeOld.getCreatedAt());
			elementType.setCreatedBy(elementTypeOld.getCreatedBy());
			elementType.setModifiedAt(new Date());
			// elementType.setModifiedBy(new User());

			elementType = repository.save(elementType);
			return new ResponseEntity<ElementType>(elementType, HttpStatus.OK);
		}
	}

	/**
	 * Create ElementType.
	 */
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> create(@Valid @RequestBody ElementType elementType) {
		if (elementType.getId() != 0) {
			return new ResponseEntity<String>("{\"message\":\"id must be null or 0\"}", HttpStatus.BAD_REQUEST);
		}
		elementType.setCreatedAt(new Date());
		elementType = repository.save(elementType);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(elementType.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

}
