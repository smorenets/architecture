package org.mycity.architecture.service.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.mycity.architecture.model.entity.Floor;
import org.mycity.architecture.persistence.repository.FloorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * Rest Controller for Floor entity.
 * 
 * @author Sergey Tishchenko
 *
 */
@RestController
@RequestMapping("/api/floors")
public class FloorController {

	@Autowired
	private FloorRepository repository;

	/**
	 * Get all Floors.
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Floor>> getAll() {
		List<Floor> floors = (List<Floor>) repository.findAll();
		return new ResponseEntity<List<Floor>>(floors, HttpStatus.OK);
	}

	/**
	 * Get Floor by id.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Floor> get(@PathVariable int id) {
		Floor floor = repository.findOne(id);
		if (floor == null) {
			return new ResponseEntity<Floor>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Floor>(floor, HttpStatus.OK);
	}

	/**
	 * Delete Floor by id.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable int id) {
		if (!repository.exists(id)) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		repository.delete(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Delete all Floors.
	 */
	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteAll() {
		repository.deleteAll();
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Update existing Floor.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> update(@PathVariable int id, @Valid @RequestBody Floor floor) {
		if (floor.getId() != id) {
			return new ResponseEntity<String>("{\"message\":\"id from URL must be equal to id from the request body\"}",
					HttpStatus.BAD_REQUEST);
		}

		Floor floorOld = repository.findOne(id);
		if (floorOld == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);			
		}
		else {
			floor.setParent(floorOld.getParent());
			floor.setCreatedAt(floorOld.getCreatedAt());
			floor.setCreatedBy(floorOld.getCreatedBy());
			floor.setModifiedAt(new Date());
			// floor.setModifiedBy(new User());

			floor = repository.save(floor);
			return new ResponseEntity<Floor>(floor, HttpStatus.OK);
		}
	}

	/**
	 * Create Floor.
	 */
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> create(@Valid @RequestBody Floor floor) {
		if (floor.getId() != 0) {
			return new ResponseEntity<String>("{\"message\":\"id must be null or 0\"}", HttpStatus.BAD_REQUEST);
		}
		floor.setCreatedAt(new Date());
		floor = repository.save(floor);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(floor.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

}
