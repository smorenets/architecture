package org.mycity.architecture.service.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.mycity.architecture.model.entity.StreetType;
import org.mycity.architecture.persistence.repository.StreetTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * CRUD REST-services for StreetType entity
 * 
 */
@RestController
@RequestMapping(value = "/api/streettypes")
public class StreetTypeController {

	@Autowired
	private StreetTypeRepository repository;

	/*
	 * Retrieve all archObjectTypes
	 */	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<StreetType>> getAll() {
		System.out.println("Fetching all streetTypes");

		List<StreetType> streetTypes = (List<StreetType>) repository.findAll();
		
		return new ResponseEntity<>(streetTypes, HttpStatus.OK);
	}

	/*
	 * Retrieve single archObjectType
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<StreetType> get(@PathVariable("id") int id) {

		System.out.println("Fetching streetType with id " + id);

		StreetType streetType = repository.findOne(id);
		if (streetType == null) {
			System.out.println("StreetType with id " + id + " not found");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(streetType, HttpStatus.OK);
	}

	/*
	 * Create an archObjectType
	 */
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Void> create(@Valid @RequestBody StreetType streetType, UriComponentsBuilder ucBuilder) {

		System.out.println("Creating streetType " + streetType.getName());

		if (repository.exists(streetType.getId())) {
			System.out.println("An streetType with id " + streetType.getId() + " already exist");
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
		if (streetType.getId() != 0) {
			System.out.println("Id from the request body must be null or 0");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
			
		streetType.setCreatedAt(new Date());

		streetType = repository.save(streetType);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/api/streettypes/{id}").buildAndExpand(streetType.getId()).toUri());
		return new ResponseEntity<>(headers, HttpStatus.CREATED);
	}

	/*
	 * Update an archObjectType
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<StreetType> update(@PathVariable("id") int id, @Valid @RequestBody StreetType streetType) {

		System.out.println("Updating streetType " + id);

		if (streetType.getId() != id) {
			System.out.println("Id from the request body must be equal to id from URL");
			return new ResponseEntity<StreetType>(HttpStatus.BAD_REQUEST);
		}

		StreetType streetTypeOld = repository.findOne(id);
		if (streetTypeOld == null) {
			System.out.println("StreetType with id " + id + " not found");		
			return new ResponseEntity<StreetType>(HttpStatus.NOT_FOUND);			
		}
		else {
			streetType.setCreatedAt(streetTypeOld.getCreatedAt());
			streetType.setCreatedBy(streetTypeOld.getCreatedBy());
			streetType.setModifiedAt(new Date());
			// streetType.setModifiedBy(new User());
	
			streetType = repository.save(streetType);
			return new ResponseEntity<StreetType>(streetType, HttpStatus.OK);
		}	
	}

	/*
	 * Delete an archObjectType
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<StreetType> delete(@PathVariable("id") int id) {

		System.out.println("Fetching & deleting streetType with id " + id);

		if (!repository.exists(id)) {
			System.out.println("Unable to delete. StreetType with id " + id + " not found");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		repository.delete(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	/*
	 * Delete all archObjectTypes
	 */
	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<StreetType> deleteAll() {

		System.out.println("Deleting all streetTypes");
		repository.deleteAll();
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
