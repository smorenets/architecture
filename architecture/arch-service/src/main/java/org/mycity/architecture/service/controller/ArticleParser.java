package org.mycity.architecture.service.controller;

import org.mycity.architecture.model.entity.Article;

/**
 * Parser for updating Article text accordingly with Article and ArchObject data.
 * <p>
 * Article.text can contain various fields of the Article or ArchObject,
 * enclosed in tags ("[" - opening tag, "]" - closing tag). The parser replaces the
 * fields with tags on the data from the database.
 */
public interface ArticleParser {
    String OPENING_TAG = "[";
    String CLOSING_TAG = "]";

    String ARTICLE_NAME = "ArticleName";
    String ARTICLE_NOTES = "ArticleNotes";
    String ARTICLE_REFERENCES = "ArticleReferences";
    String ARTICLE_PUBLICATION_DATE = "ArticlePublicationDate";
    String ARTICLE_AUTHOR_ARTICLES = "ArticleAuthorArticles";
    String ARCH_OBJECT_NAME = "ArchObjectName";
    String ARCH_OBJECT_ADDRESSES = "ArchObjectAddresses";
    String ARCH_OBJECT_ELEMENT_TYPES = "ArchObjectElementTypes";
    String ARCH_OBJECT_ARCH_OBJECT_TYPES = "ArchObjectArchObjectTypes";
    String ARCH_OBJECT_FLOORS = "ArchObjectFloors";
    String ARCH_OBJECT_MODERN_FUNCTIONS = "ArchObjectModernFunctions";
    String ARCH_OBJECT_OLD_FUNCTIONS = "ArchObjectOldFunctions";
    String ARCH_OBJECT_PERSONS = "ArchObjectPersons";
    String ARCH_OBJECT_STATUS_TYPES = "ArchObjectStatusTypes";
    String ARCH_OBJECT_STYLES = "ArchObjectStyles";
    String ARCH_OBJECT_CONSTRUCTING_BEGIN_DATES = "ArchObjectConstructingBeginDates";
    String ARCH_OBJECT_CONSTRUCTING_END_DATES = "ArchObjectConstructingEndDates";
    String ARCH_OBJECT_RECONSTRUCTIONS = "ArchObjectReconstructions";
    String ARCH_OBJECT_DESCRIPTION = "ArchObjectDescription";
    String ARCH_OBJECT_DOCUM_NUM = "ArchObjectDocumNum";
    String ARCH_OBJECT_DOCUM_DATE = "ArchObjectDocumDate";
    String ARCH_OBJECT_MAP_LOCATION = "ArchObjectMapLocation";
    String ARCH_OBJECT_CHILDREN = "ArchObjectChildren";

    String parse(Article article);
}
