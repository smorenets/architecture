package org.mycity.architecture.service.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.mycity.architecture.model.entity.OldFunction;
import org.mycity.architecture.persistence.repository.OldFunctionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * Rest Controller for OldFunction entity.
 * 
 * @author Sergey Tishchenko
 *
 */
@RestController
@RequestMapping("/api/oldfunctions")
public class OldFunctionController {

	@Autowired
	private OldFunctionRepository repository;

	/**
	 * Get all OldFunctions.
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<OldFunction>> getAll() {
		List<OldFunction> oldFunctions = (List<OldFunction>) repository.findAll();
		return new ResponseEntity<List<OldFunction>>(oldFunctions, HttpStatus.OK);
	}

	/**
	 * Get OldFunction by id.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<OldFunction> get(@PathVariable int id) {
		OldFunction oldFunction = repository.findOne(id);
		if (oldFunction == null) {
			return new ResponseEntity<OldFunction>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<OldFunction>(oldFunction, HttpStatus.OK);
	}

	/**
	 * Delete OldFunction by id.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable int id) {
		if (!repository.exists(id)) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		repository.delete(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Delete all OldFunctions.
	 */
	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteAll() {
		repository.deleteAll();
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Update existing OldFunction.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> update(@PathVariable int id, @Valid @RequestBody OldFunction oldFunction) {
		if (oldFunction.getId() != id) {
			return new ResponseEntity<String>("{\"message\":\"id from URL must be equal to id from the request body\"}",
					HttpStatus.BAD_REQUEST);
		}

		OldFunction oldFunctionOld = repository.findOne(id);
		if (oldFunctionOld == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);			
		}
		else {
			oldFunction.setParent(oldFunctionOld.getParent());
			oldFunction.setCreatedAt(oldFunctionOld.getCreatedAt());
			oldFunction.setCreatedBy(oldFunctionOld.getCreatedBy());
			oldFunction.setModifiedAt(new Date());
			// oldFunction.setModifiedBy(new User());

			oldFunction = repository.save(oldFunction);
			return new ResponseEntity<OldFunction>(oldFunction, HttpStatus.OK);
		}
	}

	/**
	 * Create OldFunction.
	 */
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> create(@Valid @RequestBody OldFunction oldFunction) {
		if (oldFunction.getId() != 0) {
			return new ResponseEntity<String>("{\"message\":\"id must be null or 0\"}", HttpStatus.BAD_REQUEST);
		}
		oldFunction.setCreatedAt(new Date());
		oldFunction = repository.save(oldFunction);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(oldFunction.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

}
