package org.mycity.architecture.service.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	@RequestMapping(path = "/hello", produces= "text/plain; charset=UTF-8")
	public String hello(){
		return "hello";
	}

	@RequestMapping(path = "/hello-json", produces= "application/json; charset=UTF-8")	
	public Value helloJson(){
		return new Value();
	}
	
	public class Value {
		public String name = "hello";
	}
	
}
