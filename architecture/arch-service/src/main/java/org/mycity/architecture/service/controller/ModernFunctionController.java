package org.mycity.architecture.service.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.mycity.architecture.model.entity.ModernFunction;
import org.mycity.architecture.persistence.repository.ModernFunctionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * Rest Controller for ModernFunction entity.
 * 
 * @author Sergey Tishchenko
 *
 */
@RestController
@RequestMapping("/api/modernfunctions")
public class ModernFunctionController {

	@Autowired
	private ModernFunctionRepository repository;

	/**
	 * Get all ModernFunctions.
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<ModernFunction>> getAll() {
		List<ModernFunction> modernFunctions = (List<ModernFunction>) repository.findAll();
		return new ResponseEntity<List<ModernFunction>>(modernFunctions, HttpStatus.OK);
	}

	/**
	 * Get ModernFunction by id.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ModernFunction> get(@PathVariable int id) {
		ModernFunction modernFunction = repository.findOne(id);
		if (modernFunction == null) {
			return new ResponseEntity<ModernFunction>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ModernFunction>(modernFunction, HttpStatus.OK);
	}

	/**
	 * Delete ModernFunction by id.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable int id) {
		if (!repository.exists(id)) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		repository.delete(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Delete all ModernFunctions.
	 */
	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteAll() {
		repository.deleteAll();
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Update existing ModernFunction.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> update(@PathVariable int id, @Valid @RequestBody ModernFunction modernFunction) {
		if (modernFunction.getId() != id) {
			return new ResponseEntity<String>("{\"message\":\"id from URL must be equal to id from the request body\"}",
					HttpStatus.BAD_REQUEST);
		}

		ModernFunction modernFunctionOld = repository.findOne(id);
		if (modernFunctionOld == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);			
		}
		else {
			modernFunction.setParent(modernFunctionOld.getParent());
			modernFunction.setCreatedAt(modernFunctionOld.getCreatedAt());
			modernFunction.setCreatedBy(modernFunctionOld.getCreatedBy());
			modernFunction.setModifiedAt(new Date());
			// modernFunction.setModifiedBy(new User());

			modernFunction = repository.save(modernFunction);
			return new ResponseEntity<ModernFunction>(modernFunction, HttpStatus.OK);
		}
	}

	/**
	 * Create ModernFunction.
	 */
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> create(@Valid @RequestBody ModernFunction modernFunction) {
		if (modernFunction.getId() != 0) {
			return new ResponseEntity<String>("{\"message\":\"id must be null or 0\"}", HttpStatus.BAD_REQUEST);
		}
		modernFunction.setCreatedAt(new Date());
		modernFunction = repository.save(modernFunction);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(modernFunction.getId())
				.toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

}
