package org.mycity.architecture.service.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.mycity.architecture.model.entity.StatusType;
import org.mycity.architecture.persistence.repository.StatusTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * Rest Controller for StatusType entity.
 * 
 * @author Sergey Tishchenko
 *
 */
@RestController
@RequestMapping("/api/statustypes")
public class StatusTypeController {

	@Autowired
	private StatusTypeRepository repository;

	/**
	 * Get all StatusTypes.
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<StatusType>> getAll() {
		List<StatusType> statusTypes = (List<StatusType>) repository.findByParent_IdIsNull();
		return new ResponseEntity<List<StatusType>>(statusTypes, HttpStatus.OK);
	}

	/**
	 * Get StatusType by id.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<StatusType> get(@PathVariable int id) {
		StatusType statusType = repository.findOne(id);
		if (statusType == null) {
			return new ResponseEntity<StatusType>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<StatusType>(statusType, HttpStatus.OK);
	}

	/**
	 * Delete StatusType by id.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable int id) {
		if (!repository.exists(id)) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		repository.delete(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Delete all StatusTypes.
	 */
	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteAll() {
		repository.deleteAll();
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Update existing StatusType.
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> update(@PathVariable int id, @Valid @RequestBody StatusType statusType) {
		if (statusType.getId() != id) {
			return new ResponseEntity<String>("{\"message\":\"id from URL must be equal to id from the request body\"}",
					HttpStatus.BAD_REQUEST);
		}

		StatusType statusTypeOld = repository.findOne(id);
		if (statusTypeOld == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);			
		}
		else {
			statusType.setParent(statusTypeOld.getParent());
			statusType.setCreatedAt(statusTypeOld.getCreatedAt());
			statusType.setCreatedBy(statusTypeOld.getCreatedBy());
			statusType.setModifiedAt(new Date());
			// statusType.setModifiedBy(new User());

			statusType = repository.save(statusType);
			return new ResponseEntity<StatusType>(statusType, HttpStatus.OK);
		}
	}

	/**
	 * Create StatusType.
	 */
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> create(@Valid @RequestBody StatusType statusType) {
		if (statusType.getId() != 0) {
			return new ResponseEntity<String>("{\"message\":\"id must be null or 0\"}", HttpStatus.BAD_REQUEST);
		}
		statusType.setCreatedAt(new Date());
		statusType = repository.save(statusType);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(statusType.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

}
