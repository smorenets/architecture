package org.mycity.architecture.service.controller;

import org.mycity.architecture.model.entity.Article;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Implementation of ArticleParser
 */
public class ArticleParserImpl implements ArticleParser {

    @Override
    public String parse(final Article article) {
        Objects.requireNonNull(article, "Article shouldn't be null.");
        String text = article.getText();
        if (text != null && !text.isEmpty()) {
            Map<String, String> data = getArticleDataMap(article);
            int currentPos = 0;
            while (true) {
                final int start = text.indexOf(OPENING_TAG, currentPos);
                final int end = text.indexOf(CLOSING_TAG);
                if (start != -1 && end != -1) {
                    currentPos = start;
                    String key = text.substring(start + 2, end);
                    text = text.replace(OPENING_TAG + key + CLOSING_TAG, data.get(key));
                } else {
                    break;
                }
            }
            return text;
        }
        return "";
    }

    private Map<String, String> getArticleDataMap(Article article) {
        final Map<String, String> articleData = new HashMap<>();
        articleData.put(ARTICLE_NAME, article.getName());
        articleData.put(ARTICLE_NOTES, article.getNotes());
        articleData.put(ARTICLE_REFERENCES, article.getReferences());
        articleData.put(ARTICLE_PUBLICATION_DATE, article.getPublicationDate().toString());
        articleData.put(ARTICLE_AUTHOR_ARTICLES, article.getAuthorArticles().toString());
        articleData.put(ARCH_OBJECT_NAME, article.getArchObject().getName());
        articleData.put(ARCH_OBJECT_ADDRESSES, article.getArchObject().getArchObjectAddresses().toString());
        articleData.put(ARCH_OBJECT_ELEMENT_TYPES, article.getArchObject().getArchObjectElementTypes().toString());
        articleData.put(ARCH_OBJECT_ARCH_OBJECT_TYPES, article.getArchObject().getArchObjectArchObjectTypes().toString());
        articleData.put(ARCH_OBJECT_FLOORS, article.getArchObject().getArchObjectFloors().toString());
        articleData.put(ARCH_OBJECT_MODERN_FUNCTIONS, article.getArchObject().getArchObjectModernFunctions().toString());
        articleData.put(ARCH_OBJECT_OLD_FUNCTIONS, article.getArchObject().getArchObjectOldFunctions().toString());
        articleData.put(ARCH_OBJECT_PERSONS, article.getArchObject().getArchObjectPersons().toString());
        articleData.put(ARCH_OBJECT_STATUS_TYPES, article.getArchObject().getArchObjectStatusTypes().toString());
        articleData.put(ARCH_OBJECT_STYLES, article.getArchObject().getArchObjectStyles().toString());
        articleData.put(ARCH_OBJECT_CONSTRUCTING_BEGIN_DATES, article.getArchObject().getConstructingBeginDates().toString());
        articleData.put(ARCH_OBJECT_CONSTRUCTING_END_DATES, article.getArchObject().getConstructingEndDates().toString());
        articleData.put(ARCH_OBJECT_RECONSTRUCTIONS, article.getArchObject().getReconstructions().toString());
        articleData.put(ARCH_OBJECT_DESCRIPTION, article.getArchObject().getDescription());
        articleData.put(ARCH_OBJECT_DOCUM_NUM, article.getArchObject().getDocumNum());
        articleData.put(ARCH_OBJECT_DOCUM_DATE, article.getArchObject().getDocumDate().toString());
        articleData.put(ARCH_OBJECT_MAP_LOCATION, article.getArchObject().getMapLocation());
        articleData.put(ARCH_OBJECT_CHILDREN, article.getArchObject().getChildren().toString());
        return articleData;
    }
}
