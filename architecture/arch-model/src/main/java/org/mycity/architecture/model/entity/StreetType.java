package org.mycity.architecture.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * StreetType (e.g. street, boulevard, square, park).
 * 
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Table(name = "STREET_TYPE")
public class StreetType extends BaseEntity {

	private List<Address> addresses;

	@Override
	@NotNull
	@Size(max = 20)
	@Column(name = "NAME")
	public String getName() {
		return super.getName();
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "streetType")
	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}
}
