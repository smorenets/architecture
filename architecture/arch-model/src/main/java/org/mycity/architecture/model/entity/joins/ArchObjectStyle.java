package org.mycity.architecture.model.entity.joins;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.mycity.architecture.model.entity.ArchObject;
import org.mycity.architecture.model.entity.Style;

/**
 * This is entity for join table of ArchObject and Style entities. Here we
 * implement Many-to-Many association with extra columns.
 * 
 * @author Sergey Tishchenko
 *
 */
@Entity
@Table(name = "ARCH_OBJECT_STYLE")
public class ArchObjectStyle extends JoinsIdentityEntity {
	private ArchObject archObject;
	private Style style;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ARCH_OBJECT_ID", nullable = false)
	public ArchObject getArchObject() {
		return archObject;
	}

	public void setArchObject(ArchObject archObject) {
		this.archObject = archObject;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "STYLE_ID", nullable = false)
	public Style getStyle() {
		return style;
	}

	public void setStyle(Style style) {
		this.style = style;
	}

}
