package org.mycity.architecture.model.entity.joins;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.mycity.architecture.model.entity.Article;
import org.mycity.architecture.model.entity.Author;

/**
 * This is entity for join table of Author and Article entities. Here we
 * implement Many-to-Many association with extra columns.
 * 
 * @author Sergey Tishchenko
 *
 */
@Entity
@Table(name = "AUTHOR_ARTICLE")
public class AuthorArticle extends JoinsIdentityEntity {
	private Author author;
	private Article article;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "AUTHOR_ID", nullable = false)
	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ARTICLE_ID", nullable = false)
	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

}
