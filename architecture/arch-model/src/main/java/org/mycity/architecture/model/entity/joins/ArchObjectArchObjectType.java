package org.mycity.architecture.model.entity.joins;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.mycity.architecture.model.entity.ArchObject;
import org.mycity.architecture.model.entity.ArchObjectType;

/**
 * This is entity for join table of ArchObject and ArchObjectType entities. Here
 * we implement Many-to-Many association with extra columns.
 * 
 * @author Sergey Tishchenko
 *
 */
@Entity
@Table(name = "ARCH_OBJECT_ARCH_OBJECT_TYPE")
public class ArchObjectArchObjectType extends JoinsIdentityEntity {
	private ArchObject archObject;
	private ArchObjectType archObjectType;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ARCH_OBJECT_ID", nullable = false)
	public ArchObject getArchObject() {
		return archObject;
	}

	public void setArchObject(ArchObject archObject) {
		this.archObject = archObject;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ARCH_OBJECT_TYPE_ID", nullable = false)
	public ArchObjectType getArchObjectType() {
		return archObjectType;
	}

	public void setArchObjectType(ArchObjectType archObjectType) {
		this.archObjectType = archObjectType;
	}

}
