package org.mycity.architecture.model.entity.joins;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.mycity.architecture.model.entity.ArchObject;
import org.mycity.architecture.model.entity.OldFunction;

/**
 * This is entity for join table of ArchObject and OldFunction entities. Here we
 * implement Many-to-Many association with extra columns.
 * 
 * @author Sergey Tishchenko
 *
 */
@Entity
@Table(name = "ARCH_OBJECT_OLD_FUNCTION")
public class ArchObjectOldFunction extends JoinsIdentityEntity {
	private ArchObject archObject;
	private OldFunction oldFunction;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ARCH_OBJECT_ID", nullable = false)
	public ArchObject getArchObject() {
		return archObject;
	}

	public void setArchObject(ArchObject archObject) {
		this.archObject = archObject;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "OLD_FUNCTION", nullable = false)
	public OldFunction getOldFunction() {
		return oldFunction;
	}

	public void setOldFunction(OldFunction oldFunction) {
		this.oldFunction = oldFunction;
	}

}
