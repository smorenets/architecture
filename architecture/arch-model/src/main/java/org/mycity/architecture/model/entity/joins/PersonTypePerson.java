package org.mycity.architecture.model.entity.joins;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.mycity.architecture.model.entity.Person;
import org.mycity.architecture.model.entity.PersonType;

/**
 * This is entity for join table of PersonType and Person entities. Here we
 * implement Many-to-Many association with extra columns.
 * 
 * @author Sergey Tishchenko
 *
 */
@Entity
@Table(name = "PERSON_TYPE_PERSON")
public class PersonTypePerson extends JoinsIdentityEntity {
	private PersonType personType;
	private Person person;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PERSON_TYPE_ID", nullable = false)
	public PersonType getPersonType() {
		return personType;
	}

	public void setPersonType(PersonType personType) {
		this.personType = personType;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PERSON_ID", nullable = false)
	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

}
