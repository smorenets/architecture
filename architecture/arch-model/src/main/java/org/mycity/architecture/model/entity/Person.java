package org.mycity.architecture.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.mycity.architecture.model.entity.joins.ArchObjectPerson;
import org.mycity.architecture.model.entity.joins.PersonTypePerson;
import org.mycity.architecture.model.entity.joins.ReconstructionPerson;

/**
 * Represents the Person who has a relationship with the architectural object.
 * 
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Table(name = "PERSON")
public class Person extends BaseEntity {

	private String fullName;
	private List<PersonTypePerson> personTypePersons;
	private List<ReconstructionPerson> reconstructionPersons;
	private List<ArchObjectPerson> archObjectPersons;

	@Override
	@Size(max = 50)
	@Column(name = "NAME", length = 50)
	public String getName() {
		return super.getName();
	}

	@NotNull
	@Size(max = 200)
	@Column(name = "FULL_NAME")
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "person")
	public List<PersonTypePerson> getPersonTypePersons() {
		return personTypePersons;
	}

	public void setPersonTypePersons(List<PersonTypePerson> personTypePersons) {
		this.personTypePersons = personTypePersons;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "person")
	public List<ReconstructionPerson> getReconstructionPersons() {
		return reconstructionPersons;
	}

	public void setReconstructionPersons(List<ReconstructionPerson> reconstructionPersons) {
		this.reconstructionPersons = reconstructionPersons;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "person")
	public List<ArchObjectPerson> getArchObjectPersons() {
		return archObjectPersons;
	}

	public void setArchObjectPersons(List<ArchObjectPerson> archObjectPersons) {
		this.archObjectPersons = archObjectPersons;
	}

}
