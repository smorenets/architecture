package org.mycity.architecture.model.entity;

import org.mycity.architecture.model.entity.joins.AuthorArticle;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * Represents Article written by Author. It describes the ArchObject.
 * 
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Table(name = "ARTICLE")
public class Article extends BaseEntity {

	private String text;
	/**
	 * Date publication by Author
	 */
	private Date publicationDate;
	private String notes;
	private String references;
	private List<AuthorArticle> authorArticles;
	private ArchObject archObject;

	@Override
	@NotNull
	@Size(max = 200)
	@Column(name = "NAME")
	public String getName() {
		return super.getName();
	}

	@NotNull
	@Size(max = 2000)
	@Column(name = "TEXT")
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@NotNull
	@Column(name = "PUBLICATION_DATE")
	public Date getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Date creationDate) {
		this.publicationDate = creationDate;
	}

	@Size(max = 500)
	@Column(name = "NOTES")
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Size(max = 2000)
	@Column(name = "REFERENCES")
	public String getReferences() {
		return references;
	}

	public void setReferences(String references) {
		this.references = references;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "article")
	public List<AuthorArticle> getAuthorArticles() {
		return authorArticles;
	}

	public void setAuthorArticles(List<AuthorArticle> authorArticles) {
		this.authorArticles = authorArticles;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ARCH_OBJECT_ID", nullable = false)
	public ArchObject getArchObject() {
		return archObject;
	}

	public void setArchObject(ArchObject archObject) {
		this.archObject = archObject;
	}

}
