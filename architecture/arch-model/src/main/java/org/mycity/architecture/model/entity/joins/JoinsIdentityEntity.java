package org.mycity.architecture.model.entity.joins;

import java.util.Date;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.mycity.architecture.model.entity.IdentityEntity;
import org.mycity.architecture.model.entity.User;

/**
 * This is super class of all joins entities. We turn off for JPA mapping
 * properties that we don't use in joins.
 * 
 * @author Nikolay Koretskyy
 *
 */
@MappedSuperclass
public class JoinsIdentityEntity extends IdentityEntity {

	@Override
	@Transient
	public Date getModifiedAt() {
		return super.getModifiedAt();
	}

	@Override
	@Transient
	public User getModifiedBy() {
		return super.getModifiedBy();
	}

}
