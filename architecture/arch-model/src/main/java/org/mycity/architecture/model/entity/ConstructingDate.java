package org.mycity.architecture.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Date of building or reconstructing ArchObject
 * 
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Table(name = "CONSTRUCTING_DATE")
public class ConstructingDate extends IdentityEntity {

	private DatePart datePart;
	/**
	 * value can contain year or century (depends on DateAbbreviation)
	 */
	private String value;
	private DateAbbreviation abbreviation;
	private String event;
	private ArchObject archObjectBegin;
	private ArchObject archObjectEnd;
	private Reconstruction reconstructionBegin;
	private Reconstruction reconstructionEnd;

	@Enumerated(EnumType.STRING)
	@Column(name = "DATE_PART", length = 20, nullable = false)
	public DatePart getDatePart() {
		return datePart;
	}

	public void setDatePart(DatePart datePart) {
		this.datePart = datePart;
	}

	@NotNull
	@Size(max = 100)
	@Column(name = "VALUE")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "DATE_ABBREV", length = 20, nullable = false)
	public DateAbbreviation getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(DateAbbreviation abbreviation) {
		this.abbreviation = abbreviation;
	}

	@Column(name = "EVENT")
	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ARCH_OBJECT_BEGIN_ID")
	public ArchObject getArchObjectBegin() {
		return archObjectBegin;
	}

	public void setArchObjectBegin(ArchObject archObjectBegin) {
		this.archObjectBegin = archObjectBegin;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ARCH_OBJECT_END_ID")
	public ArchObject getArchObjectEnd() {
		return archObjectEnd;
	}

	public void setArchObjectEnd(ArchObject archObjectEnd) {
		this.archObjectEnd = archObjectEnd;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "RECONSTRUCTION_BEGIN_ID")
	public Reconstruction getReconstructionBegin() {
		return reconstructionBegin;
	}

	public void setReconstructionBegin(Reconstruction reconstructionBegin) {
		this.reconstructionBegin = reconstructionBegin;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "RECONSTRUCTION_END_ID")
	public Reconstruction getReconstructionEnd() {
		return reconstructionEnd;
	}

	public void setReconstructionEnd(Reconstruction reconstructionEnd) {
		this.reconstructionEnd = reconstructionEnd;
	}

	public enum DatePart {
		EXACTLY, APPROX, BEGIN, MIDDLE, END, FIRST_HALF, SECOND_HALF
	}

	public enum DateAbbreviation {
		Y, YY, YEAR, CENTURY, CENTYRIES, C, CC
	}
}
