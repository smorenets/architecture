package org.mycity.architecture.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.mycity.architecture.model.entity.joins.AuthorArticle;

/**
 * The Authors, who can write articles about objects.
 * 
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Table(name = "AUTHOR")
public class Author extends BaseEntity {

	private String email;
	private List<AuthorArticle> authorArticles;

	@Override
	@NotNull
	@Size(max = 250)
	@Column(name = "NAME", unique = true)
	public String getName() {
		return super.getName();
	}

	@Size(max = 50)
	@Column(name = "EMAIL", unique = true)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "author")
	public List<AuthorArticle> getAuthorArticles() {
		return authorArticles;
	}

	public void setAuthorArticles(List<AuthorArticle> authorArticles) {
		this.authorArticles = authorArticles;
	}

}
