package org.mycity.architecture.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.mycity.architecture.model.entity.joins.ArchObjectArchObjectType;
import org.mycity.architecture.model.entity.serializer.ParentSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * The type of an architectural object (e.g. house, shop, hotel, restaurant,
 * railway station).
 * 
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Table(name = "ARCH_OBJECT_TYPE")
public class ArchObjectType extends BaseParentEntity<ArchObjectType> {

	private List<ArchObjectArchObjectType> archObjectArchObjectTypes;

	public ArchObjectType() {
		super();
	}

	public ArchObjectType(int id, String name) {
		this.setId(id);
		this.setName(name);
	}

	public ArchObjectType(String name) {
		this.setName(name);
	}

	@Override
	@NotNull
	@Size(max = 80)
	@Column(name = "NAME")
	public String getName() {
		return super.getName();
	}

	@Override
	@JsonSerialize(using = ParentSerializer.class)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PARENT_ID")
	public ArchObjectType getParent() {
		return super.getParent();
	}

	@Override
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
	public List<ArchObjectType> getChildren() {
		return super.getChildren();
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "archObjectType")
	public List<ArchObjectArchObjectType> getArchObjectArchObjectTypes() {
		return archObjectArchObjectTypes;
	}

	public void setArchObjectArchObjectTypes(List<ArchObjectArchObjectType> archObjectArchObjectTypes) {
		this.archObjectArchObjectTypes = archObjectArchObjectTypes;
	}

}
