package org.mycity.architecture.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.mycity.architecture.model.entity.joins.ArchObjectStyle;
import org.mycity.architecture.model.entity.serializer.ParentSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * This class contains the Style of architectural object. (Baroque, Renaissance,
 * Classicism, Neo-Gothic etc).
 * 
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Table(name = "STYLE")
public class Style extends BaseParentEntity<Style> {

	private List<ArchObjectStyle> archObjectStyles;

	@Override
	@NotNull
	@Size(max = 80)
	@Column(name = "NAME")
	public String getName() {
		return super.getName();
	}

	@Override
	@JsonSerialize(using = ParentSerializer.class)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PARENT_ID")
	public Style getParent() {
		return super.getParent();
	}

	@Override
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
	public List<Style> getChildren() {
		return super.getChildren();
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "style")
	public List<ArchObjectStyle> getArchObjectStyles() {
		return archObjectStyles;
	}

	public void setArchObjectStyles(List<ArchObjectStyle> archObjectStyles) {
		this.archObjectStyles = archObjectStyles;
	}

}
