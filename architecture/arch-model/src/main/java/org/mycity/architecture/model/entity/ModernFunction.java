package org.mycity.architecture.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.mycity.architecture.model.entity.joins.ArchObjectModernFunction;
import org.mycity.architecture.model.entity.serializer.ParentSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Represents a modern functional use of an architectural object (e.g. museum,
 * club, restaurant).
 * 
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Table(name = "MODERN_FUNCTION")
public class ModernFunction extends BaseParentEntity<ModernFunction> {

	private List<ArchObjectModernFunction> archObjectModernFunctions;

	@Override
	@NotNull
	@Size(max = 200)
	@Column(name = "NAME")
	public String getName() {
		return super.getName();
	}

	@Override
	@JsonSerialize(using = ParentSerializer.class)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PARENT_ID")
	public ModernFunction getParent() {
		return super.getParent();
	}

	@Override
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
	public List<ModernFunction> getChildren() {
		return super.getChildren();
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "modernFunction")
	public List<ArchObjectModernFunction> getArchObjectModernFunctions() {
		return archObjectModernFunctions;
	}

	public void setArchObjectModernFunctions(List<ArchObjectModernFunction> archObjectModernFunctions) {
		this.archObjectModernFunctions = archObjectModernFunctions;
	}

}
