package org.mycity.architecture.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.mycity.architecture.model.entity.joins.ArchObjectElementType;
import org.mycity.architecture.model.entity.serializer.ParentSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * The type of an architectural element (e.g. sculpture, fence, gate, tram
 * station).
 * 
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Table(name = "ELEMENT_TYPE")
public class ElementType extends BaseParentEntity<ElementType> {

	private List<ArchObjectElementType> archObjectElementTypes;

	@Override
	@NotNull
	@Size(max = 80)
	@Column(name = "NAME")
	public String getName() {
		return super.getName();
	}

	@Override
	@JsonSerialize(using = ParentSerializer.class)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PARENT_ID")
	public ElementType getParent() {
		return super.getParent();
	}

	@Override
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
	public List<ElementType> getChildren() {
		return super.getChildren();
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "elementType")
	public List<ArchObjectElementType> getArchObjectElementTypes() {
		return archObjectElementTypes;
	}

	public void setArchObjectElementTypes(List<ArchObjectElementType> archObjectElementTypes) {
		this.archObjectElementTypes = archObjectElementTypes;
	}

}
