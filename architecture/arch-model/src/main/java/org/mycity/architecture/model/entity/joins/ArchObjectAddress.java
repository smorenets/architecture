package org.mycity.architecture.model.entity.joins;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.mycity.architecture.model.entity.Address;
import org.mycity.architecture.model.entity.ArchObject;

/**
 * This is entity for join table of ArchObject and Address entities. Here we
 * implement Hibernate Many-to-Many association with extra columns. Solution:
 * Using a separate primary key for the join table.
 * 
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Table(name = "ARCH_OBJECT_ADDRESS")
public class ArchObjectAddress extends JoinsIdentityEntity {

	private ArchObject archObject;
	private Address address;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ARCH_OBJECT_ID", nullable = false)
	public ArchObject getArchObject() {
		return archObject;
	}

	public void setArchObject(ArchObject archObject) {
		this.archObject = archObject;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ADDRESS_ID", nullable = false)
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}
