package org.mycity.architecture.model.entity.joins;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.mycity.architecture.model.entity.ArchObject;
import org.mycity.architecture.model.entity.Floor;

/**
 * This is entity for join table of ArchObject and Floor entities. Here we
 * implement Many-to-Many association with extra columns.
 * 
 * @author Sergey Tishchenko
 *
 */
@Entity
@Table(name = "ARCH_OBJECT_FLOOR")
public class ArchObjectFloor extends JoinsIdentityEntity {
	private ArchObject archObject;
	private Floor floor;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ARCH_OBJECT_ID", nullable = false)
	public ArchObject getArchObject() {
		return archObject;
	}

	public void setArchObject(ArchObject archObject) {
		this.archObject = archObject;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FLOOR_ID", nullable = false)
	public Floor getFloor() {
		return floor;
	}

	public void setFloor(Floor floor) {
		this.floor = floor;
	}

}
