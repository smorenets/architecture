package org.mycity.architecture.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.mycity.architecture.model.entity.joins.ArchObjectStatusType;
import org.mycity.architecture.model.entity.serializer.ParentSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * This class describes the type of object value (historical, architectural,
 * archaeological etc) and sub type of object value (National, local etc) as a
 * parent.
 * 
 * @author Nikolay Koretskyy
 * 
 */
@Entity
@Table(name = "STATUS_TYPE")
public class StatusType extends BaseParentEntity<StatusType> {

	private List<ArchObjectStatusType> archObjectStatusTypes;

	@Override
	@NotNull
	@Size(max = 80)
	@Column(name = "NAME")
	public String getName() {
		return super.getName();
	}

	@Override
	@JsonSerialize(using = ParentSerializer.class)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PARENT_ID")
	public StatusType getParent() {
		return super.getParent();
	}

	@Override
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
	public List<StatusType> getChildren() {
		return super.getChildren();
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "statusType")
	public List<ArchObjectStatusType> getArchObjectStatusTypes() {
		return archObjectStatusTypes;
	}

	public void setArchObjectStatusTypes(List<ArchObjectStatusType> archObjectStatusTypes) {
		this.archObjectStatusTypes = archObjectStatusTypes;
	}

}
