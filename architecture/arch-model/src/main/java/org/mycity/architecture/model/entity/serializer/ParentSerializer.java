package org.mycity.architecture.model.entity.serializer;

import java.io.IOException;

import org.mycity.architecture.model.entity.BaseParentEntity;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class ParentSerializer extends JsonSerializer<BaseParentEntity<?>> {

	@Override
	public void serialize(BaseParentEntity<?> parent, JsonGenerator generator, SerializerProvider provider) throws IOException,
			JsonProcessingException {
		generator.writeObject(parent.getId());
	}

}
