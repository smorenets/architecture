package org.mycity.architecture.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.mycity.architecture.model.entity.joins.ArchObjectOldFunction;
import org.mycity.architecture.model.entity.serializer.ParentSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Represents first purpose of building (e.g. residential, office, service).
 * 
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Table(name = "OLD_FUNCTION")
public class OldFunction extends BaseParentEntity<OldFunction> {

	private List<ArchObjectOldFunction> archObjectOldFunctions;

	@Override
	@NotNull
	@Size(max = 200)
	@Column(name = "NAME")
	public String getName() {
		return super.getName();
	}

	@Override
	@JsonSerialize(using = ParentSerializer.class)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PARENT_ID")
	public OldFunction getParent() {
		return super.getParent();
	}

	@Override
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
	public List<OldFunction> getChildren() {
		return super.getChildren();
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "oldFunction")
	public List<ArchObjectOldFunction> getArchObjectOldFunctions() {
		return archObjectOldFunctions;
	}

	public void setArchObjectOldFunctions(List<ArchObjectOldFunction> archObjectOldFunctions) {
		this.archObjectOldFunctions = archObjectOldFunctions;
	}

}
