package org.mycity.architecture.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "USER")
public class User extends BaseEntity {

	private String email;
	private String password;

	@Override
	@NotNull
	@Size(max = 250)
	@Column(name = "NAME")
	public String getName() {
		return super.getName();
	}

	@NotNull
	@Size(max = 50)
	@Column(name = "EMAIL", unique = true)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@NotNull
	@Size(max = 50)
	@Column(name = "PASSWORD")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
