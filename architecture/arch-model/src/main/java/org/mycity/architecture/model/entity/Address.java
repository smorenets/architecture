package org.mycity.architecture.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.mycity.architecture.model.entity.joins.ArchObjectAddress;
import org.mycity.architecture.model.entity.serializer.ParentSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Represents destination of ArchObject entity. May have a description (if exact
 * address is absent). Field parent means old address.
 * 
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Table(name = "ADDRESS")
public class Address extends BaseParentEntity<Address> {

	private int zip;
	private City city;
	private String streetName;
	private StreetType streetType;
	private String number;
	private String appartment;
	private String description;
	private List<ArchObjectAddress> archObjectAddresses;

	@NotNull
	@Min(0)
	@Max(99999)
	@Column(name = "ZIP", columnDefinition = "INT(5)")
	public int getZip() {
		return zip;
	}

	public void setZip(int zip) {
		this.zip = zip;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CITY_ID", nullable = false)
	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	@NotNull
	@Size(max = 80)
	@Column(name = "STREET_NAME")
	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "STREET_TYPE_ID", nullable = false)
	public StreetType getStreetType() {
		return streetType;
	}

	public void setStreetType(StreetType streetType) {
		this.streetType = streetType;
	}

	@Size(max = 10)
	@Column(name = "NUMBER")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Size(max = 10)
	@Column(name = "APPARTMENT")
	public String getAppartment() {
		return appartment;
	}

	public void setAppartment(String appartment) {
		this.appartment = appartment;
	}

	@Size(max = 2000)
	@Column(name = "DESCRIPTION", length = 2000)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	@JsonSerialize(using = ParentSerializer.class)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PARENT_ID")
	public Address getParent() {
		return super.getParent();
	}

	@Override
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
	public List<Address> getChildren() {
		return super.getChildren();
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "address")
	public List<ArchObjectAddress> getArchObjectAddresses() {
		return archObjectAddresses;
	}

	public void setArchObjectAddresses(List<ArchObjectAddress> archObjectAddresses) {
		this.archObjectAddresses = archObjectAddresses;
	}

}
