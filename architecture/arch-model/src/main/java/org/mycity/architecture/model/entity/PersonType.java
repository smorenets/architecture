package org.mycity.architecture.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.mycity.architecture.model.entity.joins.PersonTypePerson;
import org.mycity.architecture.model.entity.serializer.ParentSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * The type of the Person (e.g. architect, doctor, singer, politician)
 * 
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Table(name = "PERSON_TYPE")
public class PersonType extends BaseParentEntity<PersonType> {

	private List<PersonTypePerson> personTypePersons;

	@Override
	@NotNull
	@Size(max = 200)
	@Column(name = "NAME")
	public String getName() {
		return super.getName();
	}

	@Override
	@JsonSerialize(using = ParentSerializer.class)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PARENT_ID")
	public PersonType getParent() {
		return super.getParent();
	}

	@Override
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
	public List<PersonType> getChildren() {
		return super.getChildren();
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "personType")
	public List<PersonTypePerson> getPersonTypePersons() {
		return personTypePersons;
	}

	public void setPersonTypePersons(List<PersonTypePerson> personTypePersons) {
		this.personTypePersons = personTypePersons;
	}

}
