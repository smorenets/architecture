package org.mycity.architecture.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.mycity.architecture.model.entity.joins.ArchObjectFloor;
import org.mycity.architecture.model.entity.serializer.ParentSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Represents floor, the basement and the mezzanine.
 * 
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Table(name = "FLOOR")
public class Floor extends BaseParentEntity<Floor> {

	private List<ArchObjectFloor> archObjectFloors;

	@Override
	@NotNull
	@Size(max = 80)
	@Column(name = "NAME")
	public String getName() {
		return super.getName();
	}

	@Override
	@JsonSerialize(using = ParentSerializer.class)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PARENT_ID")
	public Floor getParent() {
		return super.getParent();
	}

	@Override
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
	public List<Floor> getChildren() {
		return super.getChildren();
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "floor")
	public List<ArchObjectFloor> getArchObjectFloors() {
		return archObjectFloors;
	}

	public void setArchObjectFloors(List<ArchObjectFloor> archObjectFloors) {
		this.archObjectFloors = archObjectFloors;
	}

}
