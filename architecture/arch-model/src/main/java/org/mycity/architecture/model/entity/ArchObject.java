package org.mycity.architecture.model.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.mycity.architecture.model.entity.joins.ArchObjectAddress;
import org.mycity.architecture.model.entity.joins.ArchObjectArchObjectType;
import org.mycity.architecture.model.entity.joins.ArchObjectElementType;
import org.mycity.architecture.model.entity.joins.ArchObjectFloor;
import org.mycity.architecture.model.entity.joins.ArchObjectModernFunction;
import org.mycity.architecture.model.entity.joins.ArchObjectOldFunction;
import org.mycity.architecture.model.entity.joins.ArchObjectPerson;
import org.mycity.architecture.model.entity.joins.ArchObjectStatusType;
import org.mycity.architecture.model.entity.joins.ArchObjectStyle;
import org.mycity.architecture.model.entity.serializer.ParentSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * This is the main entity. ArchObjekt can present an architectural object and
 * element as well. It depends on the value of the property element. The parent
 * field defines relation to the parent object.
 * 
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Table(name = "ARCH_OBJECT")
public class ArchObject extends BaseParentEntity<ArchObject> {

	private String description;
	private String mapLocation;
	private boolean element;
	private Date documDate;
	private String documNum;
	private List<ConstructingDate> constructingBeginDates;
	private List<ConstructingDate> constructingEndDates;
	private List<Reconstruction> reconstructions;
	private List<ArchObjectAddress> archObjectAddresses;
	private List<Article> articles;
	private List<ArchObjectArchObjectType> archObjectArchObjectTypes;
	private List<ArchObjectElementType> archObjectElementTypes;
	private List<ArchObjectStyle> archObjectStyles;
	private List<ArchObjectFloor> archObjectFloors;
	private List<ArchObjectOldFunction> archObjectOldFunctions;
	private List<ArchObjectModernFunction> archObjectModernFunctions;
	private List<ArchObjectStatusType> archObjectStatusTypes;
	private List<ArchObjectPerson> archObjectPersons;

	@Override
	@NotNull
	@Size(max = 250)
	@Column(name = "NAME")
	public String getName() {
		return super.getName();
	}

	@Size(max = 2000)
	@Column(name = "DESCRIPTION")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@NotNull
	@Size(max = 2048)
	@Column(name = "MAP_LOCATION")
	public String getMapLocation() {
		return mapLocation;
	}

	public void setMapLocation(String mapLocation) {
		this.mapLocation = mapLocation;
	}

	@NotNull
	@Column(name = "ELEMENT")
	public boolean isElement() {
		return element;
	}

	public void setElement(boolean element) {
		this.element = element;
	}

	@NotNull
	@Column(name = "DOCUM_DATE")
	public Date getDocumDate() {
		return documDate;
	}

	public void setDocumDate(Date documDate) {
		this.documDate = documDate;
	}

	@NotNull
	@Size(max = 20)
	@Column(name = "DOCUM_NUM")
	public String getDocumNum() {
		return documNum;
	}

	public void setDocumNum(String documNum) {
		this.documNum = documNum;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "archObjectBegin")
	public List<ConstructingDate> getConstructingBeginDates() {
		return constructingBeginDates;
	}

	public void setConstructingBeginDates(List<ConstructingDate> constructingBeginDates) {
		this.constructingBeginDates = constructingBeginDates;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "archObjectEnd")
	public List<ConstructingDate> getConstructingEndDates() {
		return constructingEndDates;
	}

	public void setConstructingEndDates(List<ConstructingDate> constructingEndDates) {
		this.constructingEndDates = constructingEndDates;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "archObject")
	public List<Reconstruction> getReconstructions() {
		return reconstructions;
	}

	public void setReconstructions(List<Reconstruction> reconstructions) {
		this.reconstructions = reconstructions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "archObject")
	public List<ArchObjectAddress> getArchObjectAddresses() {
		return archObjectAddresses;
	}

	public void setArchObjectAddresses(List<ArchObjectAddress> archObjectAddresses) {
		this.archObjectAddresses = archObjectAddresses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "archObject")
	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "archObject")
	public List<ArchObjectArchObjectType> getArchObjectArchObjectTypes() {
		return archObjectArchObjectTypes;
	}

	public void setArchObjectArchObjectTypes(List<ArchObjectArchObjectType> archObjectArchObjectTypes) {
		this.archObjectArchObjectTypes = archObjectArchObjectTypes;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "archObject")
	public List<ArchObjectElementType> getArchObjectElementTypes() {
		return archObjectElementTypes;
	}

	public void setArchObjectElementTypes(List<ArchObjectElementType> archObjectElementTypes) {
		this.archObjectElementTypes = archObjectElementTypes;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "archObject")
	public List<ArchObjectStyle> getArchObjectStyles() {
		return archObjectStyles;
	}

	public void setArchObjectStyles(List<ArchObjectStyle> archObjectStyles) {
		this.archObjectStyles = archObjectStyles;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "archObject")
	public List<ArchObjectFloor> getArchObjectFloors() {
		return archObjectFloors;
	}

	public void setArchObjectFloors(List<ArchObjectFloor> archObjectFloors) {
		this.archObjectFloors = archObjectFloors;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "archObject")
	public List<ArchObjectOldFunction> getArchObjectOldFunctions() {
		return archObjectOldFunctions;
	}

	public void setArchObjectOldFunctions(List<ArchObjectOldFunction> archObjectOldFunctions) {
		this.archObjectOldFunctions = archObjectOldFunctions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "archObject")
	public List<ArchObjectModernFunction> getArchObjectModernFunctions() {
		return archObjectModernFunctions;
	}

	public void setArchObjectModernFunctions(List<ArchObjectModernFunction> archObjectModernFunctions) {
		this.archObjectModernFunctions = archObjectModernFunctions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "archObject")
	public List<ArchObjectStatusType> getArchObjectStatusTypes() {
		return archObjectStatusTypes;
	}

	public void setArchObjectStatusTypes(List<ArchObjectStatusType> archObjectStatusTypes) {
		this.archObjectStatusTypes = archObjectStatusTypes;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "archObject")
	public List<ArchObjectPerson> getArchObjectPersons() {
		return archObjectPersons;
	}

	public void setArchObjectPersons(List<ArchObjectPerson> archObjectPersons) {
		this.archObjectPersons = archObjectPersons;
	}

	@Override
	@JsonSerialize(using = ParentSerializer.class)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PARENT_ID")
	public ArchObject getParent() {
		return super.getParent();
	}

	@Override
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
	public List<ArchObject> getChildren() {
		return super.getChildren();
	}	

}
