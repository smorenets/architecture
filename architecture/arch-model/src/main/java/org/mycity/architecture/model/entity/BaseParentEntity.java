package org.mycity.architecture.model.entity;

import java.util.List;

/**
 * This class contains attribute <em>parent</em> for some entities.
 * 
 * @author Nikolay Koretskyy
 *
 * @param <T>
 *            Class of current entity
 */

public abstract class BaseParentEntity<T extends BaseParentEntity<T>> extends BaseEntity {

	private T parent;
	private List<T> children;

	public T getParent() {
		return parent;
	}

	public void setParent(T parent) {
		this.parent = parent;
	}

	public List<T> getChildren() {
		return children;
	}

	public void setChildren(List<T> children) {
		this.children = children;
	}

}
