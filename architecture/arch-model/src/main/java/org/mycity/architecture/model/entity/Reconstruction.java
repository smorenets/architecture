package org.mycity.architecture.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.mycity.architecture.model.entity.joins.ReconstructionPerson;

/**
 * Represents reconstruction of the architectural object.
 * 
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Table(name = "RECONSTRUCTION")
public class Reconstruction extends BaseEntity {

	private String description;
	private List<ConstructingDate> reconstructingBeginDates;
	private List<ConstructingDate> reconstructingEndDates;
	private List<ReconstructionPerson> reconstructionPersons;
	private ArchObject archObject;

	@Override
	@NotNull
	@Size(max = 200)
	@Column(name = "NAME")
	public String getName() {
		return super.getName();
	}

	@Size(max = 2000)
	@Column(name = "DESCRIPTION")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "reconstructionBegin")
	public List<ConstructingDate> getReconstructingBeginDates() {
		return reconstructingBeginDates;
	}

	public void setReconstructingBeginDates(List<ConstructingDate> reconstructingBeginDates) {
		this.reconstructingBeginDates = reconstructingBeginDates;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "reconstructionEnd")
	public List<ConstructingDate> getReconstructingEndDates() {
		return reconstructingEndDates;
	}

	public void setReconstructingEndDates(List<ConstructingDate> reconstructingEndDates) {
		this.reconstructingEndDates = reconstructingEndDates;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "reconstruction")
	public List<ReconstructionPerson> getReconstructionPersons() {
		return reconstructionPersons;
	}

	public void setReconstructionPersons(List<ReconstructionPerson> reconstructionPersons) {
		this.reconstructionPersons = reconstructionPersons;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ARCH_OBJECT_ID", nullable = false)
	public ArchObject getArchObject() {
		return archObject;
	}

	public void setArchObject(ArchObject archObject) {
		this.archObject = archObject;
	}

}
