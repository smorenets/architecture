package org.mycity.architecture.model.entity.joins;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.mycity.architecture.model.entity.Person;
import org.mycity.architecture.model.entity.Reconstruction;

/**
 * This is entity for join table of Reconstruction and Person entities. Here we
 * implement Many-to-Many association with extra columns.
 * 
 * @author Sergey Tishchenko
 *
 */
@Entity
@Table(name = "RECONSTRUCTION_PERSON")
public class ReconstructionPerson extends JoinsIdentityEntity {
	private Reconstruction reconstruction;
	private Person person;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "RECONSTRUCTION_ID", nullable = false)
	public Reconstruction getReconstruction() {
		return reconstruction;
	}

	public void setReconstruction(Reconstruction reconstruction) {
		this.reconstruction = reconstruction;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PERSON_ID", nullable = false)
	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

}
