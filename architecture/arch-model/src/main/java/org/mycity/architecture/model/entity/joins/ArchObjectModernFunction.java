package org.mycity.architecture.model.entity.joins;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.mycity.architecture.model.entity.ArchObject;
import org.mycity.architecture.model.entity.ModernFunction;

/**
 * This is entity for join table of ArchObject and ModernFunction entities. Here
 * we implement Many-to-Many association with extra columns.
 * 
 * @author Sergey Tishchenko
 *
 */
@Entity
@Table(name = "ARCH_OBJECT_MODERN_FUNCTION")
public class ArchObjectModernFunction extends JoinsIdentityEntity {
	private ArchObject archObject;
	private ModernFunction modernFunction;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ARCH_OBJECT_ID", nullable = false)
	public ArchObject getArchObject() {
		return archObject;
	}

	public void setArchObject(ArchObject archObject) {
		this.archObject = archObject;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "MODERN_FUNCTION", nullable = false)
	public ModernFunction getModernFunction() {
		return modernFunction;
	}

	public void setModernFunction(ModernFunction modernFunction) {
		this.modernFunction = modernFunction;
	}

}
