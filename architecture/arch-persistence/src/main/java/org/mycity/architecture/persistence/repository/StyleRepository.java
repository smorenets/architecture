package org.mycity.architecture.persistence.repository;

import org.mycity.architecture.model.entity.Style;
import org.springframework.data.repository.CrudRepository;

public interface StyleRepository extends CrudRepository<Style, Integer> {

}
