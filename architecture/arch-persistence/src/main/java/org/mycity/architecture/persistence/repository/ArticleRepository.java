package org.mycity.architecture.persistence.repository;

import org.mycity.architecture.model.entity.Article;
import org.springframework.data.repository.CrudRepository;

public interface ArticleRepository extends CrudRepository<Article, Integer> {

}
