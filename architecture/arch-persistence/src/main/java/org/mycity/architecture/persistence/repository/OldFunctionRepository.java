package org.mycity.architecture.persistence.repository;

import org.mycity.architecture.model.entity.OldFunction;
import org.springframework.data.repository.CrudRepository;

public interface OldFunctionRepository extends CrudRepository<OldFunction, Integer> {

}
