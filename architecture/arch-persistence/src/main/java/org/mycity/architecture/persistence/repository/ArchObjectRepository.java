package org.mycity.architecture.persistence.repository;

import org.mycity.architecture.model.entity.ArchObject;
import org.springframework.data.repository.CrudRepository;

public interface ArchObjectRepository  extends CrudRepository<ArchObject, Integer> {
}
