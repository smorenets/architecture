package org.mycity.architecture.persistence.repository;

import org.mycity.architecture.model.entity.StreetType;
import org.springframework.data.repository.CrudRepository;

public interface StreetTypeRepository extends CrudRepository<StreetType, Integer> {

}
