package org.mycity.architecture.persistence.repository;

import org.mycity.architecture.model.entity.Floor;
import org.springframework.data.repository.CrudRepository;

public interface FloorRepository extends CrudRepository<Floor, Integer> {

}
