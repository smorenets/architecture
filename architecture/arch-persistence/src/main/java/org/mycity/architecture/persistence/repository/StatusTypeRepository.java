package org.mycity.architecture.persistence.repository;

import java.util.List;

import org.mycity.architecture.model.entity.StatusType;
import org.springframework.data.repository.CrudRepository;

public interface StatusTypeRepository extends
		CrudRepository<StatusType, Integer> {
	
	List<StatusType> findByParent_IdIsNull();

}
