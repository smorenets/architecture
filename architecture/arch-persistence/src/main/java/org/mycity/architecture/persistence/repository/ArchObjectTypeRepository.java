package org.mycity.architecture.persistence.repository;

import org.mycity.architecture.model.entity.ArchObjectType;
import org.springframework.data.repository.CrudRepository;

public interface ArchObjectTypeRepository extends CrudRepository<ArchObjectType, Integer> {

}
