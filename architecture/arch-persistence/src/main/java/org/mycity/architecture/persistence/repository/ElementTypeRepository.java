package org.mycity.architecture.persistence.repository;

import org.mycity.architecture.model.entity.ElementType;
import org.springframework.data.repository.CrudRepository;

public interface ElementTypeRepository extends CrudRepository<ElementType, Integer> {

}
