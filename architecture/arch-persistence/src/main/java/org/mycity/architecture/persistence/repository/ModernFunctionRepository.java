package org.mycity.architecture.persistence.repository;

import org.mycity.architecture.model.entity.ModernFunction;
import org.springframework.data.repository.CrudRepository;

public interface ModernFunctionRepository extends
		CrudRepository<ModernFunction, Integer> {

}
